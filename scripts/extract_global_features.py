'''
Author : Kardo aziz and Victor Trejo
Description: This script computes the global features and return them as one vector
				Global features such as : mean-x, mean-y , covariance ,  number of traces,
		 									aspect ratio, line length
'''
import numpy as np
import math
from math import atan2, degrees
import entities
import normalizing_points_coordinates as npc
import project_constants as pc
import utilities as utl 




def getMeanX(traces):
	"""
	calculate mean X between all points in the symbol

	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: the mean value of x.
	:rtype: float.

	"""
	total =  0
	points = 0.0
	for t in traces:
		points += len(t.getPoints())
		for p in t.getPoints():
			total += p.x
	mean = total/points
	return mean




def getMeanY(traces):
	"""
	calculate mean Y between all points in the symbol.

	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: the mean value of y.
	:rtype: float.

	"""
	total =  0
	points = 0.0
	for t in traces:
		points += len(t.getPoints())
		for p in t.getPoints():
			total += p.y
	mean = total/points
	return mean




def getAspectRatio(traces):
	"""
	calculate Aspect Ratio in the symbol
	
	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: the aspect ratio.
	:rtype: float.

	"""
	maximumX = float("-inf")
	minimumX = float("inf")
	maximumY = float("-inf")
	minimumY = float("inf")

	for t in traces:
		for p in t.getPoints():
			maximumX = max(maximumX,p.x)
			maximumY = max(maximumY,p.y)
			minimumX = min(minimumX,p.x)
			minimumY = min(minimumY,p.y)

	# Smoother value to avoid division by zero error
	smoother = 0.1
	return ( (maximumY- minimumY) + smoother)/((maximumX - minimumX) + smoother)




def getCovariance(traces, meanx, meany):
	"""
	Calculates Covariance between all points in the symbol

	:param traces :  traces of the symbol
	:type traces: list of Trace objects.

	:param meanX :  mean x value of the symbol
	:type meanX: float

	:param meanY :  mean y value of the symbol
	:type meanY: float
	
	:return: the covariance.
	:rtype: float.

	"""
	total = 0
	points = 0.0
	for t in traces:
		points += len(t.getPoints())
		for p in t.getPoints():
			total += (p.x-meanx) * (p.y-meany)
	covariance = (total/points)
	
	return covariance



def getLineLength(traces):
	"""
	Calculates length of the traces in the symbol.

	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: length stats of the symbol.
	:rtype: a list of float.

	"""
	total = 0.0

	for t in traces:
		p = t.getPoints()
		for i in range(0, len(p)-1):
			total = total + (    \
				math.pow((p[i].x-p[i+1].x), 2)  +  \
				math.pow((p[i].y-p[i+1].y), 2)    ) ** .5
	return total




def __printGrid(grid, dimension):
	"""
	Helper function to print the grid representation of a symbol.

	:param grid :  grid representation.
	:type grid: matrix.

	:param dimension :  dimension of the grid.
	:type dimension: int.	
	"""
	print "\n\n"
	for row in range(dimension):
		for col in range(dimension):
			print '0' if grid[row, col] > 0 else '.',
		print ""





def getCrossingFeatures(traces):
	"""
	Calculates the crossing features statistics of the symbol.

	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: crossing statistics of the symbol.
	:rtype: matrix.
	
	"""
	BOX_SIZE = 4.0
	regionsPerAxis = 5
	numberOfSubcrossings = 9
	regionSize = BOX_SIZE/regionsPerAxis
	gridDimension = regionsPerAxis * numberOfSubcrossings

	CROSSING_COUNT_INDEX = 0
	FIRST_CROSSING_POSSITION_INDEX = 1
	LAST_CROSSING_POSSITION_INDEX = 2
	FEATURES_PER_REGION = 3

	xCrossingFeatures = np.zeros((regionsPerAxis, FEATURES_PER_REGION))
	yCrossingFeatures = np.zeros((regionsPerAxis, FEATURES_PER_REGION))
	subCrossingOffSet = regionSize/numberOfSubcrossings


	allPoints = [p for t in traces for p in t.getPoints() ]
	grid = np.zeros((gridDimension, gridDimension)).astype(int)

	for p in allPoints:
		gridX =  min(abs(int(p.x/subCrossingOffSet)), gridDimension - 1)
		gridY =  min(abs(int(p.y/subCrossingOffSet)), gridDimension - 1)
		grid[gridY, gridX] = 1

	
	# __printGrid(\
	# 	grid,
	# 	gridDimension
	# )
	

	def updateCrossing(\
		crossingVector,
		regionIndex,
		averageCrossing,
		averageFirstCrossingPosition,
		averageLastCrossingPosition
	):
		# possition of first intersection
		crossingVector[regionIndex, FIRST_CROSSING_POSSITION_INDEX] = averageFirstCrossingPosition
		# increasing crossing counts
		crossingVector[regionIndex, CROSSING_COUNT_INDEX] = averageCrossing
		# possition of last intersection
		crossingVector[regionIndex, LAST_CROSSING_POSSITION_INDEX] = averageLastCrossingPosition

	def crossingStats(grid, crossingFeatureVector):
		"""
		Computes the crossing statistics for axis regions.
		"""
		calAverage = lambda x: float(sum(x))/max(1, len(x))

		# Iterating over all the axis regions.
		for regionIndex in range(regionsPerAxis):

			crossingCounts = []
			firstCrossings, lastCrossings = [], []
			
			# Iterating over all the subcrossings of the region.
			for subcrossingIndex in range(numberOfSubcrossings):
				
				# calculating the subcrossing line index on the grid
				lineIndex = regionIndex * regionsPerAxis + subcrossingIndex
				
				# find the intersection points between the subcrossing line
				# and symbol's traces 
				crossings = np.where(grid[lineIndex, :] > 0)[0]

				# Adding the number of intersections found
				crossingCounts.append(len(crossings))
				
				# If there are crossings in a subcrossing line 
				# add the values for the first and last position of the crossings.
				if len(crossings) > 0:
					firstCrossings.append(crossings[0])
					lastCrossings.append(crossings[-1])

			# Computing statistics average values
			averageCrossing = calAverage(crossingCounts)
			averageFirstCrossingPosition = calAverage(firstCrossings)
			averageLastCrossingPosition = calAverage(lastCrossings)
			
			# Updating feature vector for the corresponding region
			updateCrossing(\
				crossingFeatureVector,
				regionIndex,
				averageCrossing,
				averageFirstCrossingPosition,
				averageLastCrossingPosition
			)
		

	# Computing stats for each axis
	crossingStats(grid, yCrossingFeatures)
	crossingStats(grid.T, xCrossingFeatures)
	
	# Returning the axis crossing features
	return np.hstack(\
		(
			xCrossingFeatures.flat,
			yCrossingFeatures.flat
		)
	)




def getAngularChanges(points):
	"""
	Calculates the total angular change among a set of symbols.
	
	:param points :  points to find the total angular change.
	:type points: list Point objects.
	
	:return: the computed total angular change.
	:rtype: list of float.

	"""
	slopeAngles = [\
		points[i].slopeAngle(points[i+1])
		for i in range(len(points) - 1)
	]

	return [\
		abs(slopeAngles[i] - slopeAngles[i+1])
		for i in range(len(slopeAngles) - 1)
	]




def getAngularChangeFeatures(traces):
	"""
	Calculates the angular change features a symbol.
	
	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: the computed features.
	:rtype: matrix object.

	"""
	allAngularChanges = []
	for t in traces:
		allAngularChanges.extend(\
			getAngularChanges(t.getPoints())
		)

	averageAngularChange = np.average(allAngularChanges) if len(allAngularChanges) > 0 else 0.0
	sumAngularChange = np.sum(allAngularChanges) if len(allAngularChanges) > 0 else 0.0
	
	return np.hstack(\
		(
			sumAngularChange, 
			averageAngularChange
		)
	)



def getSharpPointsFeatures(traces):
	"""
	Calculates the sharp point features a symbol.
	
	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: the computed features.
	:rtype: matrix object.

	"""

	# Finding the number of sharp points by trace
	tracesSharpPoints = []
	for t in traces:
		tracesSharpPoints.append(
			len(npc.detectSharpPoints(t.getPoints()))
		)

	# Computing the average number of sharp points for the symbol
	averageNumberOfSharpPoints = np.average(tracesSharpPoints)\
								 if len(tracesSharpPoints) > 0 else 0.0
	# Computing the sum number of sharp points for the symbol
	sumNumberOfSharpPoints = np.sum(tracesSharpPoints)\
								 if len(tracesSharpPoints) > 0 else 0.0

	# Returning the computed values
	return np.hstack(\
		(
			sumNumberOfSharpPoints, 
			averageNumberOfSharpPoints
		)
	)	


def getGlobalFeatures(traces):
	"""
	Calculates Global features for a symbol.
	
	:param traces :  traces of the symbol
	:type traces: list of Trace objects.
	
	:return: the computed features.
	:rtype: matrix object.

	"""
	# Number of traces of the symbol
	traceCount = len(tuple(traces))
	# Mean coordinate x value
	meanX = getMeanX(traces)
	# Mean coordinate y value
	meanY = getMeanY(traces)
	# Aspect ratio of the symbol
	aspect = getAspectRatio(traces)
	# Covariance of x and y coordinate values
	covariance = getCovariance(traces, meanX, meanY)
	# Symbol's length
	lineLength = getLineLength(traces)
	# Symbols crossing features
	crossing = getCrossingFeatures(traces)
	# Symbol's angular change stats
	angularChangeFeatures = getAngularChangeFeatures(traces)
	# Symbol's number of sharp points stats
	sharpPointsFeatures = getSharpPointsFeatures(traces)

	return np.hstack([\
		traceCount,
		meanX,
		meanY,
		aspect,
		covariance,
		lineLength,
		crossing,
		angularChangeFeatures,
		sharpPointsFeatures
	])

			
				
def __runTests():
	"""
	Runs some test on the extraction of the global features
	"""

	location = "{}{}".format(\
		pc.RAW_TRAIN_DATA_FOLDER,
		'65_maira.inkml'
	)

	content = utl.readFileFromLocation(location)
	expression = utl.parseINKML(content)
	symbols = list(expression.getSymbols())
	
	for symbol in symbols:
		traces = symbol.getTraces()
		npc.preprocess(traces)
		getGlobalFeatures(traces)


if __name__ == '__main__':
	__runTests()



