"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script finds all the unique symbols classes
			 in all the mathetical expressions
			 and stored them in to a json file.
"""
import project_constants as pc
import utilities as utl

def getAllUniqueSymbolsFromDirectory(directory):
	"""
	Gets all the unique symbols classes.
	
	:param directory: location of the directory that 
					  contains the inkml files whose
					  the symbols are going to be extracted
					  from.
	:type directory: string.
	:return: a set of all the symbols classes in the directory.
	:rtype: set of string objects.
	"""
	getFileLocation = lambda f: "{}{}".format(directory, f)
	allDirectorySymbols = []
	for f in utl.directoryFiles(directory):
		allDirectorySymbols.extend(\
			utl.getAllSymbolsFromInkml
			(
				utl.readFileFromLocation
				(
					getFileLocation(f)
				)
			)
		)
	return set(allDirectorySymbols)


def main():
	print("Getting all unique symbol labels from training")
	symbolsInTraining = getAllUniqueSymbolsFromDirectory(\
		pc.RAW_TRAIN_DATA_FOLDER
	)

	print("Getting all unique symbol labels from testing")
	symbolsInTesting = getAllUniqueSymbolsFromDirectory(\
		pc.RAW_TEST_DATA_FOLDER
	)
	
	print("Combining all unique symbols")
	allSymbols = symbolsInTraining.union(symbolsInTesting)
	
	print("Storing all unique symbols at {}".format(pc.ALL_UNIQUE_SYMBOL_CLASSES_FILE))
	utl.saveJsonAtLocation(\
		sorted(allSymbols),
		pc.ALL_UNIQUE_SYMBOL_CLASSES_FILE
	)

if __name__ == "__main__":
	main()