"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script generates the output lg graphs for all the data sets: train, test, whole.
"""

import os, ntpath
import project_constants as pc
import utilities as utl

def generateLGFiles(listOfFiles, datasetType):
	"""
	Generates the lg files for a list of inkml files for a dataset type.

	:param listOfFiles: list of file locations to generate the lg files from.
	:type listOfFiles: list of string.

	:param datasetType: dataset type.
	:type datasetType: string.

	"""
	
	utl.cleanDirectory(pc.GROUND_TRUTH_LG_FOLDER(datasetType))
	utl.createDirectory(pc.GROUND_TRUTH_LG_FOLDER(datasetType))
	
	for inputFileLocation in listOfFiles:
		fileName = ntpath.basename(inputFileLocation).replace(".inkml", "")
		outputFile = pc.GROUND_TRUTH_LG_OUTPUT_FILE(datasetType, fileName)
		os.system("crohme2lg -s '{}' '{}'".format(inputFileLocation, outputFile))



def main():
	"""
	Generates the lg files for a list of inkml files for each dataset type.

	"""
	print("Generating Ground Truth Output graphs ...")
	
	trainFiles = map(\
		lambda f: pc.TRAIN_INKML_FILE(f),
		utl.directoryFiles(pc.RAW_TRAIN_DATA_FOLDER)
	)
	testFiles = map(\
		lambda f: pc.TEST_INKML_FILE(f),
		utl.directoryFiles(pc.RAW_TEST_DATA_FOLDER)
	)

	generateLGFiles(\
		trainFiles,
		'train'
	)
	generateLGFiles(\
		testFiles,
		'test'
	)

	generateLGFiles(\
		set(trainFiles).union(testFiles),
		'whole'
	)

if __name__ == "__main__":
	main()
	