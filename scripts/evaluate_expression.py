"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script is used to evaluate a single expression on the trained
classifiers.
"""
import numpy as np
import utilities  as utl
import project_constants as pc
import result as rs
import entities, shutil, os, sys
import segmentation as seg
import Parsing



def evaluate(features, mappings, onWholeData, directoryFunction):
	"""
	Evaluates the expression extracted features on the classifiers.
	
	:param features :  features extracted from expression
	:type features: matrix.
	
	:param mappings : expression metadata to generate the lg graph.
	:type mappings: list of dictionary.

	:param onWholeData : Flag to indicate whether to use the classifiers
						used in the whole data or not.
	:type onWholeData: boolean.

	"""

	Parsing.initializeModel(onWholeData)

	for classifierInformation in utl.getClassifiersInformation():

		print("Loading {} model\n".format(\
			classifierInformation.name)
		)

		preFix = 'whole_' if onWholeData else ''
		modelName = "{}{}".format(\
			preFix,
			classifierInformation.name.lower()
		)

		model = utl.loadModel(modelName)
		
		print(\
			"Testing {}{}classifier on expression".format(
				classifierInformation.name,
				' (whole data trained) ' if onWholeData else ' '
			)
		)

		
		results = map(
			lambda n:entities.getLabelForClassNumber(n),
			model.predict(features[:, :-1])
		)

		directory = directoryFunction(modelName)
		
		utl.createDirectory(directory)

		for mapping in mappings:
			
			rs.saveResultToLG(\
				"{}/{}.lg".format(directory, mapping['name']),
				mapping['symbols'],
				results
			)




def main(\
	location,
	evaluateOnWholeData,
	segmentationModel,
	debug = False,
	directoryFunction = pc.LG_SINGLE_EXPRESSIONS_OUTPUT_FOLDER,
	performSegmentation = True
):
	"""
	Parses the file at the given location, extract features and,
	classifies its symbols.
	:param location :  location of the file.
	:type location: string.
	
	:param evaluateOnWholeData : to indicate if the model trained on the whole 
								 data is going to be used.
	:type evaluateOnWholeData: boolean.

	:param segmentationModel : Model to be used for segmentation to find symbols probabilities.
	:type segmentationModel: classification model.

	:param debug : To set print debugging or not.
	:type debug: boolean.

	"""
	# Initizializing segmentation classification model
	if performSegmentation:
		seg.initialize(segmentationModel)

	# Setting debug flag to debug or not each segmentation step
	seg.debug = debug
	#From a list of inkml expression files, parses them,
	# apply segmentation and then extract features.
	if performSegmentation:
		allFeatures, mappings = utl.extracFeaturesFromFilesLocations(\
			[location],
			inkmlFileParser = seg.parseINKMLWithCustomSegmentation
		)
	else:
		allFeatures, mappings = utl.extracFeaturesFromFilesLocations(\
			[location]
		)
	# Evaluate the list of extracted features corresponding to symbol 
	# in the set of different classification models.
	evaluate(\
		np.vstack(allFeatures),
		mappings,
		evaluateOnWholeData,
		directoryFunction
	)




if __name__ == "__main__":
	
	if len(sys.argv) > 1:
		location = sys.argv[1]
		evaluateUsingClassifierTrainedOnWholeData = 'whole' in sys.argv
		performSegmentation = not 'dnps' in sys.argv

		debug = 'debug' in sys.argv
		
		segmentationModel = None
		
		if performSegmentation:
			if evaluateUsingClassifierTrainedOnWholeData:
				segmentationModel = utl.loadModel(\
					pc.WHOLE_DATA_SEGMENTATION_CLASSIFIER_ID
				)

			else:
				# Loading segmentation model
				segmentationModel = utl.loadModel(\
					pc.SEGMENTATION_CLASSIFIER_ID
				)



		main(\
			location,
			evaluateUsingClassifierTrainedOnWholeData,
			segmentationModel,
			debug,
			pc.LG_SINGLE_EXPRESSIONS_OUTPUT_FOLDER,
			performSegmentation
		)

		