"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script uses grid search to find the best parameters to tune our classifier.
"""

from sklearn import ensemble, grid_search
import numpy as np
import utilities  as utl
import project_constants as pc


def crossValidate(parameters):
	"""
	Applies grid search to find the best model to solve the classification
	task.
	:param parameters: parameters to try.
	:type parameters: a dictionary with the list of parameters to try in the search.

	"""
	classifier = ensemble.RandomForestClassifier()
	searcher = grid_search.GridSearchCV(\
		classifier,
		parameters
	)
	
	trainData = np.load(pc.PROCESSED_TRAIN_DATA_SET_FILE)
	testData = np.load(pc.PROCESSED_TEST_DATA_SET_FILE)

	print("Starting Search")
	inputs, outputs = trainData[:, :-1], trainData[:, -1:].ravel()
	searcher.fit(inputs, outputs)

	score = searcher.score(\
		testData[:, :-1],
		testData[:, -1:].ravel()
	)
	print("Score = {}".format(score)) 
	print("Grid Scores = {}\n".format(searcher.grid_scores_))
	print("Best Params = {}\n".format(searcher.best_params_))
	print("Best Score = {}".format(searcher.best_score_))



if __name__ == "__main__":
	# Parameters to try
	parameters = {\
		'n_estimators' : np.arange(50, 70, 2) ,
		'max_depth'  :  np.arange(10,20,2)
	}
	crossValidate(parameters)









	# 0.90207960336
	# Grid Scores = [mean: 0.85573, std: 0.00034, params: {'n_estimators': 10}, 
	#mean: 0.87603, std: 0.00308, params: {'n_estimators': 20}, mean: 0.88613, std: 0.00166, 
	#params: {'n_estimators': 30}, mean: 0.89006, std: 0.00189, params: {'n_estimators': 40}, 
	#mean: 0.89391, std: 0.00105, params: {'n_estimators': 50}]
	# Best Params = {'n_estimators': 50}
	# Best Score = 0.893912706521