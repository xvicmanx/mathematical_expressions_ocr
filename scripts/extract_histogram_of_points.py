"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script extracts features information
			 of the symbols from the expressions on the testing set
			 and store it.
"""

import math


def getMembership(point,corner,width,height):
	"""
	This function computes membership of a point to a corner
	:param point: the point
	:param corner: the corner
	:param width: is the width of a cell in the grid
	:param height: is the height of a cell in the grid

	:return one value which is membership calculate by using following equation
		given a Point(Xp, Yp)
		and  a corner(Xc, Yc)
		membership = width-(Xp-Xc)/width * height-(Yp-Yc)/height
	"""
	membership = (width-(abs(point.x-corner[0])))/width * \
				  (height-(abs(point.y-corner[1])))/height
	return membership

def getDistance(point,corner):
	"""
	this Function calculate distance between a corner and a point
	:param point: the point that we want to find its membership
	:param corner: the corner that we check with the point
	:return : one value which is distance between the point and the corner
	"""
	
	return [abs(point.x-corner[0]),abs(point.y-corner[1])]

def getHistogramofPoints(traces):
	"""
	This function calculate the histogram pf points by dividing the symbole into
	a grid of 4X4 which gives us 25 corners, then for each corner compute membership of points around it 
	"""
	minX = float("inf")
	minY = float("inf")
	maxX = float("-inf")
	maxY = float("-inf")

	for t in traces:
		for p in t.getPoints():
			minX = min(minX,p.x)
			minY = min(minY,p.y)
			maxX = max(maxX,p.x)
			maxY = max(maxY,p.x)
	
	# leap is the distance between corners
	leap = 1.0
	# print("{} {} {} {}".format(minX,maxX,minY,maxY))
	# print("Leap X : {}".format(leap))
	# print("Leap Y : {}".format(leap))
	# if(maxX == minX or maxY == minY) :
	# 	print("Straight Lineeeeeee : {}, {}, {}, {}".format(minX,maxX,minY,maxY))
	# each corner has three values :
	# first value is X poistin
	# second value is Y position
	# third value is sum of all memberships
	corners = [[[0.0, 0.0, 0.0], \
	 [0.0, 0.0, 0.0],  \
	 [0.0, 0.0, 0.0],  \
	 [0.0, 0.0, 0.0],  \
	 [0.0, 0.0, 0.0]] for i in range(0, 5)]

	for i in range (0,5):
		for j in range(0,5):
			corners[i][j][0] = (i*leap)			
			corners[i][j][1] = (j*leap)

	points = 0.0
	# for Each trace
	for t in traces:
		# get points in the trace
		point = t.getPoints()
		# find number of points in the symbole by adding up number of point in each trace
		points += len(point)

		for p in point:
			# check if the point is belong to how many corners
			# and find its membership with that corner
			for i in range(0,5):
				for j in range(0,5):
					c = corners[i][j]
					# find distance of the point to the corner
					distance = getDistance(p,c)
					# if both x and y are smaller than leap it means it is a member of that corner
					

					if distance[0]<=leap and distance[1]<=leap :
						# then find its memebership to the corner
						c[2] += getMembership(p,c,leap,leap)
	# finally we have to divide the memberships by total number of points in the symbol			
	


	cornerVector = []
	for i in range(0,5):
		for j in range(0,5):
			c = corners[i][j]
			cornerVector.append( c[2] /points)

	return cornerVector



if __name__ == "__main__":
	"""
	this was used for testing
	To be removed at the end of the project
	"""

	# p1 = Point(0.5,0.5)
	# p2 = Point(0.5,1.5)
	# p3 = Point(0.5,2.5)
	# p4 = Point(0.5,3.5)
	# p5 = Point(1.5,0.5)
	# p6 = Point(1.5,1.5)
	# p7 = Point(1.5,2.5)
	# p8 = Point(1.5,3.5)
	# p9 = Point(2.5,0.5)
	# p10 = Point(2.5,1.5)
	# p11 = Point(2.5,2.5)
	# p12 = Point(2.5,3.5)
	# p13 = Point(3.5,0.5)
	# p14 = Point(3.5,1.5)
	# p15 = Point(3.5,2.5)
	# p16 = Point(3.5,3.5)
	# points = [p1,p2,p3,p4,\
	# 		p5,p6,p7,p8,\
	# 		p9,p10,p11,p12,\
	# 		p13,p14,p15,p16]
	# t = Trace(0,points)
	# getHistogramofPoints([t])

	#print(getDistance(p1,[1,2]))









