"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script performs the following tasks:
				1. Training classification models.
				2. Generating output graphs of classifiers on datasets.
				3. Generating evaluation of the classifiers output graphs
				   against the ground truths graphs.

"""

import train_classification_models
import generate_classifiers_output_graphs_on_datasets
import generate_output_evaluation
import sys

def main(onWholeData = False):
	"""
	Performs the following tasks:
		1. Training classification models.
		2. Generating output graphs of classifiers on datasets.
		3. Generating evaluation of the classifiers output graphs
		   against the ground truths graphs.

	:param onWholeData :  if the whole training data is also
	going to be used to train the models.
	:type onWholeData: boolean

	"""
	
	train_classification_models.main(onWholeData)
	generate_classifiers_output_graphs_on_datasets.main()
	generate_output_evaluation.main()

if __name__ == "__main__":
	onWholeData = 'whole' in sys.argv
	main(onWholeData)