import sys
import evaluate_expression as evex
import utilities as utl
import project_constants as pc

def main(alsoEvaluateTrain, debug = False, ):

	utl.customPrint("Loading model...", debug)

	model = utl.loadModel(pc.SEGMENTATION_CLASSIFIER_ID)
	testFiles = utl.getTestingFilePaths()
	count = 0
	print("Evaluating testing set")
	for testFile in testFiles:
		count+=1
		utl.customPrint(\
			"{} - Evaluating expression at {}".format(count, testFile),
			debug
		)
		evex.main(\
			testFile,
			False,
			model,
			debug,
			pc.LG_SEGMENTATION_PLUS_CLASSIFICATION_OUTPUT_FOLDER('test')
		)
		
	if alsoEvaluateTrain:
		count = 0
		trainFiles = utl.getTrainingFilePaths()
		print("Evaluating traing set")
		for trainFile in trainFiles:
			count+=1
			utl.customPrint(\
				"{} - Evaluating expression at {}".format(count, trainFile),
				debug
			)
			evex.main(\
				trainFile,
				False,
				model,
				debug,
				pc.LG_SEGMENTATION_PLUS_CLASSIFICATION_OUTPUT_FOLDER('train')
			)


if __name__ == "__main__":
	alsoEvaluateTrain = 'evaluate_train' in sys.argv
	debug = 'debug' in sys.argv
	main(True, True)

		