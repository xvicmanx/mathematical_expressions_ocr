"""
Authors: Kardo Aziz and Victor Trejo.
Description: This file contains general purpose functions that are used throughout the project.
"""

import numpy as np
import math

def __getLeftSidesDifference(\
	firstSymbol,
	secondSymbol,
	width
):
	box1 = firstSymbol.getBox()
	box2 = secondSymbol.getBox()
	return (box2.minPoint.x - box1.minPoint.x)/\
			width

def __getRightSidesDifference(\
	firstSymbol,
	secondSymbol,
	width
):
	box1 = firstSymbol.getBox()
	box2 = secondSymbol.getBox()
	return (box2.maxPoint.x - box1.maxPoint.x)/\
			width

def __getTopSidesDifference(\
	firstSymbol,
	secondSymbol,
	height
):
	box1 = firstSymbol.getBox()
	box2 = secondSymbol.getBox()
	return (box2.minPoint.y - box1.minPoint.y)/\
			height

def __getBottomSidesDifference(\
	firstSymbol,
	secondSymbol,
	height
):
	box1 = firstSymbol.getBox()
	box2 = secondSymbol.getBox()
	return (box2.maxPoint.y - box1.maxPoint.y)/\
			height

def __getCentersXDifference(\
	firstSymbol,
	secondSymbol,
	width
):
	box1 = firstSymbol.getBox()
	box2 = secondSymbol.getBox()
	return (box2.center.x - box1.center.x)/\
			width

def __getCentersYDifference(\
	firstSymbol,
	secondSymbol,
	height
):
	box1 = firstSymbol.getBox()
	box2 = secondSymbol.getBox()
	return (box2.center.y - box1.center.y)/\
			height

def getSlope(firstSymbol, secondSymbol):
	b1 = firstSymbol.getBox()
	b2 = secondSymbol.getBox()
	dx = b1.center.x - b2.center.x
	dy = b1.center.y - b2.center.y

	if dx == 0:
		return 90
	return math.atan(dy/dx) * 180/math.pi



def extractFeatures(firstSymbol, secondSymbol):
	
	box1 = firstSymbol.getBox()
	box2 = secondSymbol.getBox()
	
	width = max(box1.maxPoint.x, box2.maxPoint.x) - \
			min(box1.minPoint.x, box2.minPoint.x)
	width = width if width > 0.0 else 1.0

	height = max(box1.maxPoint.y, box2.maxPoint.y) - \
			min(box1.minPoint.y, box2.minPoint.y)
	height = height if height > 0.0 else 1.0
					
	leftSidesDifference = __getLeftSidesDifference(\
		firstSymbol,
		secondSymbol,
		width
	)
	
	rightSidesDifference = __getRightSidesDifference(\
		firstSymbol,
		secondSymbol,
		width
	)
	
	topSidesDifference = __getTopSidesDifference(\
		firstSymbol,
		secondSymbol,
		height
	)
	
	bottomSidesDifference = __getBottomSidesDifference(\
		firstSymbol,
		secondSymbol,
		height
	)
	
	centersXDifference = __getCentersXDifference(\
		firstSymbol,
		secondSymbol,
		width
	)

	centersYDifference = __getCentersYDifference(\
		firstSymbol,
		secondSymbol,
		height
	)

	angle = getSlope(\
		firstSymbol,
		secondSymbol
	)

	return np.hstack([\
		leftSidesDifference,
		rightSidesDifference,
		bottomSidesDifference,
		topSidesDifference,
		centersXDifference,
		centersYDifference,
		angle
	])

