"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script trains a classifier using the features information
			 of the symbols from the expressions on the training set.
"""
import utilities  as utl
import project_constants as pc
import numpy as np
import sys

def __trainOnData(data, classifierInformation, label):
	"""
	Trains a classifier on a given data.

	:param data :  training data
	:type data: matrix.

	:param classifierInformation : classifier metadata
	:type classifierInformation: utl.ClassifierInformation.
	

	:param label : printing label
	:type label: string

	:return: the train model.
	:rtype: Classifier.

	"""

	rows, cols = data.shape
	
	model = classifierInformation.classifierClass(\
		**classifierInformation.params
	)

	print("\nTraining {} classifier".format(label))

	model.fit(\
		data[:, :-1],
		data[:, -1:].ravel()
	)

	return model



def __saveModel(model, location, label):
	"""
	Saves a model at a given location.

	:param model :  model to be stored.
	:type model: Classifier

	:param location : location to store the model
	:type location: string
	

	:param label : printing label
	:type label: string

	"""

	print("Saving {} model\n".format(label))
	utl.saveModelAtLocation(model, location)



def main(onWholeData = False):
	"""
	Trains the models on training sets

	:param onWholeData :  if the whole training data is also
	going to be used to train the models.
	:type onWholeData: boolean

	"""

	dataSets = {
		'train': {
			'data' : np.load(pc.PROCESSED_TRAIN_DATA_SET_FILE),
			'prefix':''
		}
	}

	if onWholeData:
		
		trainingFeaturesData = np.load(pc.PROCESSED_TRAIN_DATA_SET_FILE)
		testingFeaturesData = np.load(pc.PROCESSED_TEST_DATA_SET_FILE)
		
		data = np.vstack((trainingFeaturesData, testingFeaturesData))
		
		dataSets['whole'] = {
			'data' : data,
			'prefix':'whole_'
		}


	for classifierInformation in utl.getClassifiersInformation():
		for datasetType, info in dataSets.items():

			identifier = "{}{}".format(\
				info['prefix'],
				classifierInformation.name.lower()
			)
			
			model = __trainOnData(\
				info['data'],
				classifierInformation,
				identifier
			)
			
			__saveModel(\
				model,
				pc.MODEL_FILE_NAME(identifier),
				identifier
			)
		

if __name__ == "__main__":

	onWholeData = 'whole' in sys.argv
	main(onWholeData)

	

