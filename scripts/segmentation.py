"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script contains the implementation of the segmentation step.
"""
import numpy as np
import project_constants as pc
from bs4 import BeautifulSoup
import entities
import utilities as utl
import features_extraction as fe
import strokes_grouping_features as sgf

# Flag that controls if debugging is going
# to be performed
debug = False
# To store the reference of the segmentation classification model
model = None

# Median trace length of traces
# from a given mathematical expression 
medianTraceLength = None
def segment(traces):
	"""
	Segments a sequence a traces into a meaninful 
	groups that represent symbols.
	
	:param traces: a sequence of traces.
	:type traces: list of Trace objects.

	:return: the chosen segmentation.
	:rtype: a list of tuple of traces.

	"""
	global debug
	global medianTraceLength
	
	# segmentation configuration object:
	#	Contains the parameters such as:
	#	1. segmentation score function.
	configuration = SegmentationConfig(\
		classificationScore
	)
	
	# calculating the median trace length  from all the given traces
	medianTraceLength = np.median([t.getLength() for t in traces])

	debug = True

	#utl.customPrint("Traces : {}".format(len(traces)), debug)
	#utl.customPrint("Point before {}".format(traces[0].getPoints()[0]), debug)
	
	# Finding best segmentation using dynamic programming
	segmentation = []
	scores, splits = bottomUpSegmentation(traces, configuration)
	findPartition(\
		0,
		len(traces)-1,
		splits,
		traces,
		segmentation
	)

	
	#utl.customPrint("Traces : {}".format(len(traces)), debug)
	#utl.customPrint("Best Segmentation", debug)

	# Printing chosen best segmentation
	#if debug: printSegmentation(segmentation)

	#utl.customPrint("Point after {}".format(traces[0].getPoints()[0]), debug)
	
	# returning found best segmentation
	return segmentation


def initialize(modelObject):
	"""
	Initializes segmentation scoring symbol model.

	:param modelObject: model to be set.
	:type modelObject: Scikit-learn classifier.

	"""
	global model
	model = modelObject




def bottomUpSegmentation(traces, configuration):
	"""
	Finds the best segmentation of a sequence of traces,
	using dynamic programming.

	:param traces: traces to be segmented.
	:type traces: list of Trace entities objects.

	:param configuration: segmentation configuration.
	:type configuration: SegmentationConfig.

	:return: the scores of the segmentations, and the corresponding
			 splits.
	:rtype: a pair of matrices (scores, splits).

	"""

	N = len(traces)
	# To store the scores and splits for the possible segmentations
	scores = np.zeros((N, N))
	splits = np.zeros((N, N)).astype(int)


	def calculateScore(i, j):
		"""
		Finds the score of the segmentation group for the strokes
		si, ..., sj.

		:param i: position of the starting stroke in the group.
		:type i: int.

		:param j: position of the ending stroke in the group.
		:type j: int.

		:return: the score of the strokes group.
		:rtype: float.

		"""
		# if the number of strokes in group is bigger than the 
		# maximum number of strokes per symbol, then the score 
		# is 0.0
		if len(traces[i:j+1]) > pc.MAXIMUM_STROKES_PER_SYMBOL:
			return 0.0

		# Otherwise the score of the group is calculated.
		try:
			score =  configuration.scoreGroup(\
					traces[i:j+1]
			)
		except:
			score = 0.0
		return score 


	# Calculating score for the base cases:
	# Stroke groups that have only one strokes.
	for i in range(N):
		scores[i, i] = calculateScore(i, i)
		# -1 indicates that all the traces
		# in range are taken in this case the current trace
		splits[i, i] = -1


	# Using the bottom up the segmentation for all the different
	# traces range is found until completing the segmentation for
	# the range s0, ...SN.
	# width indicates how many traces are used in the range.
	# first it starts with 1 and increasing it.
	for width in range(1, len(traces)-1):
		for start in range(0, N - width):
			
			end = start + width
			
			# Trying different splits to find
			# the best split position.
			maximumScore = float("-inf")
			bestSplit = -1
			for split in range(start, end):
				
				score = scores[start, split] * scores[split + 1, end]
				if score > maximumScore:
					maximumScore = score
					bestSplit = split

			# Finds the score as if it would be used the 
			# all the traces in range start-end
			whole = calculateScore(start, end)
			# If the score of the split is bigger or equal
			# use the the information of that split other
			# wise do not split and use all the strokes in sequence.
			if maximumScore >= whole:
				scores[start, end] = maximumScore
				splits[start, end] = bestSplit

			else:
				scores[start, end] = whole
				splits[start, end] = -1
		

	return scores, splits



# segmentation configuration object:
#	Contains the parameters such as:
#	1. segmentation score function.
class SegmentationConfig(object):
	"""docstring for SegmentationConfig"""
	def __init__(self, scoreGroup):
		super(SegmentationConfig, self).__init__()
		self.scoreGroup = scoreGroup






# To cached the probabilistic model reference.
bayesianModel = None
def classificationScore(traces):
	"""
	Calculates the score of a given traces group.

	:param traces: traces in group.
	:type traces: list of Trace entities objects.
	
	:return: the score of the strokes group.
	:rtype: float.

	"""
	global bayesianModel
	global medianTraceLength

	# Loading probabilistic model if it has not been loaded
	# before.
	if bayesianModel is None:
		bayesianModel = utl.loadModel(\
			pc.STROKES_GROUPING_PROBABILISTIC_MODEL
		)
	
	# Making a copy of the traces
	traces = [
		t.getCopy() for t in traces
	]

	# Finding the probability that 
	# the given sequence of traces in
	# group go together. This calculated 
	# as the product of the probabilities of 
	# a sequence  (stroke m and stroke + g) to be together.
	tracesTogetherProbability = 1.0
	for i in range(len(traces)):

			currentTrace = traces[i]
			rangeValue = min(\
				i + pc.STROKES_GROUPING_TO_CHECK_RANGE,
				len(traces)
			)
			
			for j in range(i+1, rangeValue):
				
				otherTrace = traces[j]
				
				# Extracting features between a pair of traces
				tracesFeatures = sgf.extractFeaturesBetweenTraces(\
					currentTrace,
					otherTrace,
					medianTraceLength
				)

				# Finding the probability of pair of traces being together
				# based on the extracted features between them
				probability = bayesianModel.predict_proba([tracesFeatures])
				#tracesTogetherProbability*= probability[0][1]


	# Treating group as a symbol and extracting features
	# from it.
	features = fe.extractAllFeatures(traces)
	# Then finding the probabilities of that group being 
	# a symbol
	symbolProbabilities = model.predict_proba([features])
	# Returning the value of the product of the probabilies
	# of the traces being together by the maximum probability
	# of that group being a symbol.
	return np.max(symbolProbabilities[0, :]) * tracesTogetherProbability




def printSegmentation(segmentation):
	"""
	Prints out a given segmentation in a beautiful format.

	:param segmentation: segmentation to be printed out.
	:type segmentation: list of tuples.

	"""

	print "--------------------------"
	print "\nSegmentation : {"
	for g in segmentation:
		print "\n\tGroup : {"
		for i in g: 
			print("\t\t{}, ".format(i))

		print "\t},"
	print "\n}"
	print "--------------------------\n"




def copySegmentation(segmentation):
	"""
	Makes a copy of a given segmentation.

	:param segmentation: segmentation to be copied.
	:type segmentation: list of tuples.

	:return: copy of the segmentation.
	:rtype: list of tuples.

	"""
	result = []
	for g in segmentation:
		group = []
		
		for i in g: group.append(i)
		
		result.append(group)

	return result





def findPartition(\
	start,
	end,
	splits,
	traces,
	solution
):
	"""
	Recursively finds the segmentation solution, using the information
	from the splits structure.


	:param start: starting position in the range.
	:type start: int.

	:param end: ending position in the range.
	:type end: int.

	:param splits: structure that was filled using the dynamic programming
					that indicates the best split for a given stroke range.
	:type splits: numpy matrix.

	:param traces: traces to be used to form the solution.
	:type traces: list of Trace entities objects.

	:param solution: where the solution is going to be stored.
	:type solution: empty list.

	"""
	if start > end: return

	split = splits[start, end]
	
	if split == -1:

		end+=1
		group = traces[start:end]

		if len(group) > 0: solution.append(group)

	else:
		if split!=end:
			findPartition(\
				start,
				split,
				splits,
				traces,
				solution
			)

		findPartition(\
			split + 1,
			end,
			splits,
			traces,
			solution
		)




def parseINKMLWithCustomSegmentation(content):
	"""
	Parses an inkml file. with custom segmentation
	
	:param content: content of the inkml file
	:type content: string
	:return: a mathematical expression object created 
			 from information of the inkml file.
	:rtype: MathematicalExpression
	"""
	inkml = BeautifulSoup(content,'lxml')
	
	expression = entities.MathematicalExpression()

	# Extracting symbols information, traces, points 
	# from the xml file.
	def mapTraceFromTag(traceTag):
		points  = [\
			entities.Point.fromString(p)
			for p in traceTag.get_text().split(", ")
		]
		return entities.Trace(\
			traceTag['id'],
			points
		)

	traces =  map(\
		mapTraceFromTag, 
		__getTraceTags(inkml)
	)

	for group in segment(traces):
		symbol = entities.Symbol('', '')
		
		for trace in group:
			symbol.addTrace(\
				trace.getId(),
				trace.getPoints()
			)	
		expression.addSymbol(symbol)

	# Returning the created mathematical expression.
	return expression


def __getTraceTags(inkml):
	"""
	Gets all the trace tags from a inkml content.
	
	:param inkml: inkml content object.
	:type inkml: BeautifulSoup.
	:return: a list of Beautiful XML tags
			 representing the trace tags.
	:rtype: Beautiful XML tag
	"""
	return inkml.find_all('trace')





if __name__ == "__main__":


	configuration = SegmentationConfig(\
		lambda s: 1.0
	)
	
	

	traces = ['s1', 's2', 's3']
	s, splits = bottomUpSegmentation(traces, configuration)
	print splits
	segmentation = []
	findPartition(\
		0,
		len(traces)-1,
		splits,
		traces,
		segmentation
	)
	printSegmentation(segmentation)
