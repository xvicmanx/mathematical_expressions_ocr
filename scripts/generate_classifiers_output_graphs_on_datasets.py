"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script test a classifier using the features information
			 of the symbols from the expressions on the test set.
"""
import numpy as np
import utilities  as utl
import project_constants as pc
import result as rs
import entities
import shutil, os
import Parsing

def saveOutputGraphs(datasetType, classifierName, results, mapping):
	"""
	Save lg files for datasets.

	:param datasetType: dataset type.
	:type datasetType: string.

	:param classifierName:  name of the classifier.
	:type classifierName: string.

	:param results:  results of the classification.
	:type results: a matrix.

	:param mapping:  expression metadata needed to generate lg graphs.
	:type mapping: list of dictionary.
	
	"""

	outputDirectory = pc.LG_OUTPUT_FOLDER(datasetType, classifierName)

	utl.cleanDirectory(outputDirectory)
	utl.createDirectory(outputDirectory)


	for formulaMapping in mapping:
		
		rs.saveResultToLG(\
			pc.LG_OUTPUT_FILE(\
				datasetType,
				classifierName,
				formulaMapping['name']
			),
			formulaMapping['symbols'],
			results
		)




def main():
	"""
	
	Generates classifiers output graph lg files from datasets.
	
	"""

	trainDatasetFeatures = np.load(pc.PROCESSED_TRAIN_DATA_SET_FILE)
	testDatasetFeatures = np.load(pc.PROCESSED_TEST_DATA_SET_FILE)
	trainingMapping = utl.readModelAtLocation(pc.TRAINING_SET_SYMBOLS_MAPPING)
	testingMapping = utl.readModelAtLocation(pc.TESTING_SET_SYMBOLS_MAPPING)
	Parsing.initializeModel(False)

	for classifierInformation in utl.getClassifiersInformation():
	
		print("Loading {} model\n".format(\
			classifierInformation.name)
		)

		model = utl.readModelAtLocation(\
			pc.MODEL_FILE_NAME(classifierInformation.name.lower())
		)
		
		print("Testing {} classifier on training set".format(\
			classifierInformation.name)
		)

		onTrainDatasetResults = map(
			lambda n:entities.getLabelForClassNumber(n),
			model.predict(trainDatasetFeatures[:, :-1])
		)

		

		print("Testing {} classifier on testing set".format(\
			classifierInformation.name)
		)

		onTestDatasetResults = map(
			lambda n:entities.getLabelForClassNumber(n),
			model.predict(testDatasetFeatures[:, :-1])
		)

		# print model.score(trainDatasetFeatures[:, :-1], trainDatasetFeatures[:, -1]) 
		# print model.score(testDatasetFeatures[:, :-1], testDatasetFeatures[:, -1]) 

		saveOutputGraphs(\
			'train',
			classifierInformation.name.lower(),
			onTrainDatasetResults,
			trainingMapping
		)

		saveOutputGraphs(\
			'test',
			classifierInformation.name.lower(),
			onTestDatasetResults,
			testingMapping
		)

if __name__ == "__main__":
	main()