"""
Authors: Kardo Aziz and Victor Trejo.
Description: This file contains entities classes and functions declarations for the current domain
			 of mathematical expressions.
"""
import utilities as utl
import json, pickle
import project_constants as pc
import numpy as np
import extract_histogram_of_orientation as slope


def readJsonAtLocation(location):
	"""
	Reads a JSON object from a file at a specific
	location.
	
	:param location: location of the JSON file to read.
	:type location: string
	
	:return: a json object created from content of the file .
	:rtype: JSON object.

	"""
	with open(location) as JSONFile:
		return json.load(JSONFile)

parsingCategories = readJsonAtLocation(\
	pc.SYMBOLS_PARSING_CATEGORIES
)


symbolParsingCategoriesMap = {}
for category in parsingCategories:
	for s in parsingCategories[category]:
		symbol = s.replace(',', 'COMMA')
		symbolParsingCategoriesMap[symbol] = category


allUniqueSymbols = None
def getSymbolMappedNumber(symbolClass):
	"""
	Gets the mapped number of a given symbol class.
	
	:param symbolClass: the symbol's string class.
	:type symbolClass: string.

	:return: the class mapped number.
	:rtype: int

	"""
	global allUniqueSymbols
	if allUniqueSymbols is None:
		allUniqueSymbols = utl.readJsonAtLocation(\
			pc.ALL_UNIQUE_SYMBOL_CLASSES_FILE
		)
		
	result = None
	if symbolClass in allUniqueSymbols:
		result = allUniqueSymbols.index(symbolClass)
	return result



class BoundingBox(object):
	"""Trace BoundingBox abstraction"""
	def __init__(self, minX, minY, maxX, maxY, centerX=0 ,centerY=0):
		super(BoundingBox, self).__init__()
		# Minimum point
		self.minPoint = Point(minX, minY)
		# Maximum point
		self.maxPoint = Point(maxX, maxY)
		# Center Point
		self.center = Point(centerX,centerY)

	def getWidth(self):
		"""
		Gets the width of the current bounding box.
		
		:return: the value of the width.
		:rtype: float.
		"""
		return self.maxPoint.x - self.minPoint.x

	def getHeight(self):
		"""
		Gets the height of the current bounding box.
		
		:return: the value of the height.
		:rtype: float.
		"""
		return self.maxPoint.y - self.minPoint.y

	def isBetweenXRange(self, x):
		return self.minPoint.x <= x < self.maxPoint.x

	def isBetweenYRange(self, y):
		return self.minPoint.y <= y < self.maxPoint.y

	def contains(self, point):
		return self.isBetweenXRange(point.x) and\
				self.isBetweenYRange(point.y)

	def __str__(self):
		"""
		Returns the representation of the current symbol.
		
		:return: string representation of the object.
		:rtype: string

		"""
		return 'Bounding box :\n {} \n {} \n {}'.format(\
			self.minPoint,self.maxPoint,self.center
		) 


	@staticmethod
	def fromTuple(box):
		"""
		Instantiates a bounding box from a given 
		tuple representing the min_x, min_y,
		max_x, max_y values of the box.
		
		:param box: values tuple to create the bounding box
					from.
		:type box: tuple of float values.

		:return: the created bounding box.
		:rtype: BoundingBox.
		"""
		return BoundingBox(\
			box[0],
			box[1],
			box[2],
			box[3],
			box[4] if len(box) > 5 else 0,
			box[5] if len(box) > 5 else 0
		)

def getLabelForClassNumber(number):
	"""
	Gets the mapped symbol class label given an id number.
	
	:param number: id number.
	:type number: int.

	:return: the mapped class label.
	:rtype: string

	"""
	global allUniqueSymbols
	if allUniqueSymbols is None:
		# Reading id-to-class-label map
		allUniqueSymbols = utl.readJsonAtLocation(\
			pc.ALL_UNIQUE_SYMBOL_CLASSES_FILE
		)
	return allUniqueSymbols[int(number)]





class MathematicalExpression:
	"""
	MathematicalExpression : Represents a 
							mathematical expression.
	"""
	def __init__(self):
		self.__symbols = []


	def addSymbol(self, symbol):
		"""
		Adds a symbol to the current mathematical expression.
		
		:param symbol: symbol to add.
		:type symbol: Symbol.
		
		"""
		self.__symbols.append(symbol)
	
	def getSymbols(self):
		"""
		Iterates over all the symbols in the current
		mathematical expression.

		:return: an iterator over all the symbols.
		:rtype: iterable of Symbol
		"""
		return self.__symbols
			

	def printContent(self):
		"""
		Prints the content of the current mathematical expression.
		"""
		for s in self.__symbols:
			print(s.getGroundTruth())
			for t in s.getTraces():
				print ("\t{}").format(t)
				for p in t.getPoints():
					print ("\t\t{}").format(p)
	
	def __str__(self):
		"""
		Returns the representation of the current mathematical 
		expression.

		:return: string representation of the object.
		:rtype: string

		"""
		return "Mathemathical Expression"



class Symbol:
	"""
	Symbol : Represents a mathematical symbol.
	"""
	def __init__(self, truth, tagLabel):
		"""
		Constructor.
		
		:param truth: symbol class.
		:type truth: string

		"""
		self.__groundTruth = truth
		self.__tagLabel = tagLabel
		self.__traces = []
		self.__box = None

	
	def getParsingCategory(self):
		global symbolParsingCategoriesMap
		symbolClass = self.__groundTruth.replace(',', 'COMMA')
		parsingCategory = symbolParsingCategoriesMap.get(symbolClass, 'Others')
		return parsingCategory

	def getBox(self):
		if(self.__box is None ):
			self.createSymbolBox()
		return self.__box

	# def setBox(self,b):
	# 	self.__box = b
	def getMinX(self):
		box = self.getBox() 
		return box.minPoint.x
		


	def createSymbolBox(self):
	#for s in symbols:
		mx = float("-inf")
		nx = float("inf")
		my = float("-inf")
		ny = float("inf")
		for t in self.__traces:
			x = [p.x for p in t.getPoints()]
			y = [p.y for p in t.getPoints()]
			mx = max(mx,max(x))
			nx = min(nx,min(x))
			my = max(my,max(y))
			ny = min(ny,min(y))
		centerx = (mx+nx)/2.0
		centery = (my+ny)/2.0
		
		self.__box = BoundingBox.fromTuple((nx,ny,mx,my,centerx,centery))
	# print(len(list(symbols)))

	def getGroundTruth(self):
		"""
		Gets the symbol's class label.

		:return: symbol's class label.
		:rtype: string

		"""
		return self.__groundTruth

	def getTag(self):
		"""
		Gets the symbol's href (tag) label.

		:return: symbol's href (tag) label.
		:rtype: string

		"""
		tagLabel = '' if self.__tagLabel is None else self.__tagLabel
		return tagLabel
	
	def addTrace(self, traceId, points):
		"""
		Adds  trace to the current symbol.
		
		:param traceId: trace's id.
		:type traceId: string.

		:param points: trace's points.
		:type points: list of Point objects.

		"""
		self.__traces.append(\
			Trace(
				traceId,
				points
			)
		)

	def getTraces(self):
		"""
		Iterates over all the traces in the current
		symbol.

		:return: an iterator over all the traces.
		:rtype: iterable of Trace

		"""
		return self.__traces


	def getClassNumber(self):
		"""
		Returns the symbol's class mapped number.
		
		:return: symbol's class mapped number.
		:rtype: int

		"""
		return getSymbolMappedNumber(\
			self.__groundTruth
		)

	def updateTraces(self, traces):
		self.__traces = traces

	def __str__(self):
		"""
		Returns the representation of the current symbol.
		
		:return: string representation of the object.
		:rtype: string

		"""
		return 'Symbol : (truth = {})'.format(\
			self.__groundTruth
		) 




class Trace:
	"""
	Trace : Represents a mathematical symbol's trace.
	"""
	def __init__(self, traceId, points):
		"""
		Constructor.
		
		:param traceId: trace's id value.
		:type traceId: string.

		:param points: trace's points.
		:type points: list of Point objects.

		"""
		self.__id = traceId
		self.__points = points

	def getId(self):
		"""
		Gets the trace's id.

		:return: trace's id.
		:rtype: string

		"""
		return self.__id
	
	def getPoints(self):
		"""
		Iterates over all the points in the current
		trace.

		:return: an iterator over all the points.
		:rtype: iterable of Point

		"""
		return self.__points

	def updatePoints(self, points):
		"""
		Updates the traces of the current trace

		:param points: new points to update.
		:type points: list of Point objects.

		"""
		self.__points = points

	def __str__(self):
		"""
		Returns the representation of the current trace.

		:return: string representation of the object.
		:rtype: string

		"""
		return 'Trace : (id = {})'.format(\
			self.__id
		)

	def getLength(self):
		points = self.__points;
		total = 0.0
		for i in range(0, len(points)-1):
			total+= points[i].distance( points[i+1] )
		return total

	def getCopy(self):
		points = []
		for p in self.__points:
			points.append(
				Point(
					p.x,
					p.y
				)
			)
		return Trace(
			self.__id,
			points
		)




class Line(object):
	"""
	Line : Represents a line in a plane.
	"""
	def __init__(self, startPoint, endPoint):
		"""
		Constructor.
		
		:param startPoint: starting point of the line.
		:type startPoint: Point.

		:param endPoint: ending point of the line.
		:type endPoint: Point.

		"""
		super(Line, self).__init__()

		self.startPoint = startPoint
		self.endPoint = endPoint


	def slopeAngle(self):
		"""
		Calculates the slope angle formed by the current line.
		
		:return: the slope angle.
		:rtype: float
		
		"""
		return self.startPoint.slopeAngle(self.endPoint)

	def angleBetween(self, otherLine):
		"""
		Calculates the angle between the current line
		and another given line.
		
		:param otherLine: the other line to find the angle between.
		:type otherLine: Line

		:return: the angle between the lines.
		:rtype: float
		
		"""
		return self.slopeAngle() - otherLine.slopeAngle()
	
	def length(self):
		"""
		Calculates length of a given line.
		
		:return: the length of the line.
		:rtype: float
		
		"""
		return self.startPoint.distance(self.endPoint)


class Point:
	"""
	Point : Represents a trace's point.
	"""
	def __init__(self, x, y):
		"""
		Constructor.
		
		:param x: point's x coordinate.
		:type x: float

		:param y: point's y coordinate.
		:type y: float

		"""
		self.x = x
		self.y = y

	@classmethod
	def fromString(cls, textString):
		"""
		Creates a point from a given string.
		The format of this string should be:  x y
		
		:param textString: point string info.
		:type textString: string

		:return: a point created from the string information.
		:rtype: Point

		"""
		tupleInstance = tuple(textString.split())
		return Point(\
			float(tupleInstance[0]),
			float(tupleInstance[1])
		)

	def update(self,x,y):
		self.x = x
		self.y = y


	def distance(self, otherPoint):
		"""
		This Function calculate the euclidean distance between the current point
		and another one.

		:param otherPoint: the point to find the distance between
		:type otherPoint: Point

		:return : distance value

		"""
		return ((self.x - otherPoint.x)**2 + (self.y - otherPoint.y)**2)**0.5

	def slope(self, otherPoint):
		"""
		Calculates the slope formed by the current point
		and another point.
		
		:param otherPoint: the point to find the slope between
		:type otherPoint: Point

		:return: the slope.
		:rtype: float
		
		"""

		deltaX = otherPoint.x - self.x
		deltaY = otherPoint.y -  self.y

		if deltaX == 0 and deltaY >= 0:
			return float("inf")
		elif deltaX == 0 and deltaY < 0:
			return float("-inf")

		return (otherPoint.y -  self.y)/(otherPoint.x - self.x)


	def __angle(self, slope):
		"""
		Calculates the corresponding angle of a slope.
		
		:param slope: the slope whose corresponding angle
						will be found.
		:type slope: float

		:return: the slope's angle.
		:rtype: float
		
		"""

		if np.isclose(slope, float("inf")):
			return np.pi/2
		elif np.isclose(slope, float("-inf")):
			return 3 * np.pi/2
		else:
			angle = np.arctan(slope)

			return  angle if angle >= 0 else angle + 2*np.pi

	def slopeAngle(self, otherPoint):
		"""
		Calculates the slope angle formed by the current point
		and another point.
		
		:param otherPoint: the point to find the slope angle between
		:type otherPoint: Point

		:return: the slope angle.
		:rtype: float
		
		"""
		slope = self.slope(otherPoint)
		return self.__angle(slope)

	

	def __str__(self):
		"""
		Returns the representation of the current point.

		:return: string representation of the object.
		:rtype: string

		"""
		return 	"Point : (x = {}, y = {})".format(\
			self.x,
			self.y
		)



	def __hash__(self):
		"""
		Calculates the hash unique hash representation of the
		current object.

		:return: hash representation.
		:rtype: number

		"""
		return hash((self.x, self.y))

	def __eq__ (self, other):
		"""
		Checks whether the current object
		is equals to another one.

		:param other: the point to compare to.
		:type other: Point

		:return: if the objects are equal or not.
		:rtype: boolean

		"""
		return np.isclose(self.x, other.x) and\
				 np.isclose(self.y, other.y)
		
	