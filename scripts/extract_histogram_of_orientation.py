"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script extracts Histogram of Orientation features 
			which divide the symbol into a grid of 2X2 then consider
			slope of the line for each corner.
"""
import entities as ent
import extract_histogram_of_points as hp
import math

def getSlope(point1,point2):
	"""
	This Function finds the slope of a line and tell to which region it belongs to
	We divide slope of the line into 4 region (0,45,90,135)
	:param point1 : is the first point of the line
	:param point2 : is the second point of the line

	:return: returns either(0,1,2,3) which tell which region this slope belongs to

	"""
	dx = point1.x - point2.x
	dy = point1.y- point2.y
	if dx == 0 :
		angle=90
	else:
		angle = math.atan(dy/dx) * 180/math.pi
	
	if angle<22.5 and angle>-22.5 :
		return 0
	elif angle>22.5 and angle<=67.5 :
		return 1
	elif angle>67.5 or angle<-67.5 :
		return 2
	else :
		return 3


def getMiddle(p1,p2):
	"""
	This Function finds the middle point of a line which is represented by two point
	:param point1 : is the first point of the line
	:param point2 : is the second point of the line

	:return : returns X and Y position of the middle point
	"""
	X = (p1.x+p2.x)/2.0
	Y = (p1.y+p2.y)/2.0
	return [X,Y]



def getlineLength(p1,p2):
	"""
	This Function finds the length of a line which is represented by two point
	:param point1 : is the first point of the line
	:param point2 : is the second point of the line

	:return : returns the length of the line
	:rtype : int value
	"""
	dx = math.pow((p1.x-p2.x),2)
	dy = math.pow((p1.y-p2.y),2)
	return math.sqrt(dx+dy)



def getHistogramofOrientation(traces):
	"""
	This function calculate the histogram pf points by dividing the symbole into
	a grid of 2X2 which gives us 9 corners, then for each corner we 
	consider 4 slope (0,45,90,135), 
	meaning each corner has 4 value. total we have 36 values
	"""

	# leap is the distance between corners
	leap = 2.0
	corners = []
	tracecount = len(tuple(traces))
	# each corner has six values :
	# first value is X poistion of the corner
	# second value is Y position of the corner
	# third value is lines with slope close to 0
	# fourth value is lines with slope close to 45
	# fifth value is lines with slope close to 90
	# sixth value is lines with slope close to 135	
	#print("creating empty corner informations")
	corners = [[[ [i*2.0, 0.0, 0.0, 0.0, 0.0, 0.0], \
		     [i*2.0, 1*leap, 0.0, 0.0, 0.0, 0.0],  \
		     [i*2.0, 2*leap, 0.0, 0.0, 0.0, 0.0]    \
		   ] for i in range(0, 3)] \
		   for j in range(0,tracecount)]
	# intial length of the symbol is zero
	totallength = 0
	# intial length of each trace = 0
	tracelength = [0 for i in range(0,tracecount)]
	trace = 0
	# then find the length of each trace
	for t in traces :
		points = t.getPoints();
		for i in range(0,len(points)-1):
			tracelength[trace] += getlineLength(points[i],points[i+1])
		# Add the length of the trace to the total length of the symbol
		totallength += tracelength[trace]
		# go to next trace
		trace += 1

	#fixining small bug
	totallength= totallength if totallength > 0 else 1.0

	points = 0.0
	trace = 0
	# Compute individual histogram for each trace
	for t in traces:
		# get points in the trace
		point = t.getPoints()
		# find number of points in the symbole by adding up number of point in each trace
		po = len(point)
		points += po
		for i in range(0,po-1):
			p1 = point[i]
			p2 = point[i+1]
			m = getMiddle(p1,p2)
			middle = ent.Point(m[0],m[1])
			length = tracelength[trace] if tracelength[trace] > 0 else 1
			linelength = getlineLength(p1, p2)/length
			slope = getSlope(p1, p2) + 2
	
			# find the membership of each line segment to the corners around it 
			# and 
			for i in range(0,3):
				for j in range(0,3):
					c = corners[trace][i][j]
					distance = hp.getDistance(middle,c)
					# if the both x and y distance are smaller than leap
					# it means that its a member of that corner
					if(distance[0]<=leap and distance[1]<=leap):
						membership = hp.getMembership(middle,c,leap,leap)
						# the slope is used to specify which the bin in that corner
						c[slope] += membership*(linelength/totallength)
		trace += 1	
	# finally we have to divide the memberships by total number of points in the symbol			
	cornerVector = []
	trace =0

	bin_values = [0 for i in range(0,36)]
	# then add up all individual histograms and make them as 1D vector 
	for c in range(0,tracecount):
		temp = (corners[c])
		_bin = 0
		for i in range(0,3):
			for j in range(0,3):
				for k in range(2,6):
					bin_values[_bin] += temp[i][j][k]
					_bin += 1

	# print(bin_values)		
	return bin_values



if __name__ == "__main__":
	"""
	this was used for testing
	To be removed at the end of the project
	"""
	p1 = ent.Point(0.5,0.5)
	p2 = ent.Point(0.5,1.5)
	p3 = ent.Point(0.5,2.5)
	p4 = ent.Point(0.5,3.5)
	p5 = ent.Point(1.5,0.5)
	p6 = ent.Point(1.5,1.5)
	p7 = ent.Point(1.5,2.5)
	p8 = ent.Point(1.5,3.5)
	p9 = ent.Point(2.5,0.5)
	p10 = ent.Point(2.5,1.5)
	p11 = ent.Point(2.5,2.5)
	p12 = ent.Point(2.5,3.5)
	p13 = ent.Point(3.5,0.5)
	p14 = ent.Point(3.5,1.5)
	p15 = ent.Point(3.5,2.5)
	p16 = ent.Point(3.5,3.5)
	points = [p1,p2,p3,p4,\
			p5,p6,p7]
	points2=[p8,p9,p10,p11,p12,\
			p13,p14,p15,p16]
	t = ent.Trace(0,points)
	t2 = ent.Trace(0,points2)
	getHistogramofOrientation([t,t2])

	# p = ent.Point(3,4)
	# print(getDistance(p,[3.0,-3.0]))
	#print(getDistance(p1,[1,2]))
