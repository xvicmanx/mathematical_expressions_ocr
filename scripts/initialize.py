"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script cread ground truth number maps, and extracts features information for the datasets.
"""

import create_ground_truth_to_number_map
import extract_testing_symbols_features
import extract_training_symbols_features
import generate_truth_output_graphs
import sys



def main(runTruthGeneratorsOfMappingAndGraphs, debug = False):
	"""
	Perform the basic initialization tasks:
		1. Creating ground truth to symbol ids map.
		2. Generating ground truth output graphs.
		3. Extracting features from training data set inkml files.
		4. Extracting features from testing data set inkml files.

	
	:param runTruthGeneratorsOfMappingAndGraphs: flag to indicate 
		if the two first initialization tasks are going to be perform or not.
	:type runTruthGeneratorsOfMappingAndGraphs: boolean.

	:param debug: flag that indicates debugging of tasks or not
	:type debug: boolean.

	"""
	if runTruthGeneratorsOfMappingAndGraphs:
		create_ground_truth_to_number_map.main()
		generate_truth_output_graphs.main()
	
	extract_training_symbols_features.main(debug)
	extract_testing_symbols_features.main(debug)

	

if __name__ == "__main__":
	runTruthGeneratorsOfMappingAndGraphs =  not 'ntg' in sys.argv
	debug = 'debug' in sys.argv
	main(runTruthGeneratorsOfMappingAndGraphs, debug)