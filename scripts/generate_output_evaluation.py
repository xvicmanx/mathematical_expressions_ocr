"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script evaluates the result of classification of a model and creates the summary file 
			in the model folder in the evaluation path
"""
import project_constants as pc
import utilities as utl
import os



def evaluate(command, datasetType, classifierName):
	"""
	Compares the lg outputs generated from the classifiers against the ground truth.

	:param command: command to perform (confHist or evaluate).
	:type command: string.

	:param datasetType: dataset type.
	:type datasetType: string.

	:param classifierName:  name of the classifier.
	:type classifierName: string.
	
	"""

	# evaluate the result of the model
	os.system(\
		command.format(
			pc.LG_OUTPUT_FOLDER(
				datasetType,
				classifierName
			)
			,
			pc.LG_OUTPUT_FOLDER(
				"ground_truth",
				datasetType
			)
		)
	)



def evaluateOnSet(datasetType):
	"""
	Compares the lg outputs generated from the classifiers against the ground truth.

	:param datasetType: dataset type.
	:type datasetType: string.
	
	"""

	outputDirectory = pc.EVALUATION_OUTPUT_FOLDER(datasetType)
		
	#change the directory to the evaluation folder
	os.chdir(outputDirectory)

	utl.createDirectory(pc.EVALUATION_OUTPUT_FOLDER(datasetType))
	utl.cleanDirectory(pc.EVALUATION_OUTPUT_FOLDER(datasetType))

	for classifierInformation in utl.getClassifiersInformation():
		
		classifierName = classifierInformation.name.lower()
	
		# evaluate the result of the model
		evaluate("evaluate {} {}", datasetType, classifierName)
		
		# confusion Histogram of the classification
		evaluate("confHist {} {} 1", datasetType, classifierName)



def main():
	"""
	
	Compares the lg outputs generated from the classifiers against the ground truth
	of each dataset.

	"""

	evaluateOnSet("test")
	evaluateOnSet("train")

		


if __name__ == "__main__":
	main()
	