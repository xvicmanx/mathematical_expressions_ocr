import project_constants as pc
import utilities as utl
import ntpath
import extract_relationship_features as ersf
from sklearn.naive_bayes import GaussianNB
import numpy as np
import sys, os, entities
from sklearn import ensemble

symbolsParsingCategories = tuple(entities.parsingCategories.keys())

def getRelationFromLGFile(location):
	relationships = []
	with open(location) as lgFile:
		for line in lgFile:
			parts = line.split(', ')
			if parts[0] == 'R':
				relationships.append(\
					tuple(parts[1:4])
				)

	return relationships


def getTrainingData(inkmlFiles, datasetType):
	global symbolsParsingCategories
	allFeatures = {cat:[] for cat in symbolsParsingCategories}
	allTargets = {cat:[] for cat in symbolsParsingCategories}
	count = 0
	for inklmlFile in inkmlFiles:
		count+=1
		expression = utl.parseINKML(\
			utl.readFileFromLocation(inklmlFile)
		)
		print("{} - {}".format(count, inklmlFile))
		
		symbolsTagMap = {
			s.getTag().replace(',', 'COMMA'):s
			for s in expression.getSymbols()
		}

		nameWithoutExtension = ntpath.basename(inklmlFile).replace('.inkml', '')
		relationships = getRelationFromLGFile(\
			pc.GROUND_TRUTH_LG_OUTPUT_FILE(
				datasetType,
				nameWithoutExtension
			)
		)

		for relationship in relationships:

			firstTag, secondTag, relation = relationship
			firstSymbol = symbolsTagMap[firstTag]
			secondSymbol = symbolsTagMap[secondTag]
			features = ersf.extractFeatures(\
				firstSymbol,
				secondSymbol
			)

			parsingCategory = firstSymbol.getParsingCategory()
			allTargets[parsingCategory].append(relation)
			allFeatures[parsingCategory].append(features)

	return allFeatures, allTargets




def main(whole):

	trainInkmlFiles = utl.getTrainingFilePaths()
	allFeatures, allTargets = getTrainingData(\
		trainInkmlFiles,
		'train'
	)

	if whole:
		testInkmlFiles = utl.getTestingFilePaths()
		allTestingFeatures, allTestingTargets = getTrainingData(\
			testInkmlFiles,
			'test'
		)
		allFeatures = {
			k:(allFeatures[k] + allTestingFeatures[k])
			for k in allFeatures
		}

		allTargets = {
			k:(allTargets[k] + allTestingTargets[k])
			for k in allTargets
		}


	
	models = {cat:None for cat in symbolsParsingCategories}
	for parsingCategory in symbolsParsingCategories:
		features = np.vstack(allFeatures[parsingCategory])
		targets = allTargets[parsingCategory]
		classifier = GaussianNB()
		# classifier = ensemble.RandomForestClassifier()
		classifier.fit(features, targets)
		models[parsingCategory] = classifier

	print("Saving Models...")
	utl.saveModelAtLocation(
		models,
		pc.PARSING_MODEL_LOCATION(whole)
	)

if __name__ == "__main__":
	whole = 'whole' in sys.argv
	main(whole)
