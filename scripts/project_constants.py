"""
Authors: Kardo Aziz and Victor Trejo.
Description: This file contains the project constants values and values functions.
"""
import os
from sklearn.neighbors import KNeighborsClassifier
from sklearn import ensemble
from collections import namedtuple
import custom_classifiers as cc
currentFileDirectory = os.path.dirname(__file__)

# identifier of classifier used to help in the segmentation process
SEGMENTATION_CLASSIFIER_ID = "random_forest_classifier"
WHOLE_DATA_SEGMENTATION_CLASSIFIER_ID = "whole_random_forest_classifier"
STROKES_GROUPING_PROBABILISTIC_MODEL = 'bayesian_classifier'
WHOLE_DATA_STROKES_GROUPING_PROBABILISTIC_MODEL = 'whole_bayesian_classifier'
STROKES_GROUPING_TO_CHECK_RANGE = 3
MAXIMUM_STROKES_PER_SYMBOL = 6

# Location of the folders that contain the Inkml files for training and testing
RAW_TRAIN_DATA_FOLDER = '{}/../data/raw/train/'.format(currentFileDirectory)
RAW_TEST_DATA_FOLDER = '{}/../data/raw/test/'.format(currentFileDirectory)

# RAW_TRAIN_DATA_FOLDER = '{}/../data/development_raw/train/'.format(currentFileDirectory)
# RAW_TEST_DATA_FOLDER = '{}/../data/development_raw/test/'.format(currentFileDirectory)

TRAIN_INKML_FILE = lambda fileName: '{}{}'.format(RAW_TRAIN_DATA_FOLDER, fileName)
TEST_INKML_FILE = lambda fileName: '{}{}'.format(RAW_TEST_DATA_FOLDER, fileName)

# Location of the files that contain the information of features extracted from symbols
# belonging to expressions of the training and testing data sets.
PROCESSED_TRAIN_DATA_SET_FILE = '{}/../data/processed/training_set.npy'.format(currentFileDirectory)
PROCESSED_TEST_DATA_SET_FILE = '{}/../data/processed/testing_set.npy'.format(currentFileDirectory)
PROCESSED_WHOLE_DATA_SET_FILE = '{}/../data/processed/whole_data_set.npy'.format(currentFileDirectory)


TESTING_SET_SYMBOLS_MAPPING  = '{}/../data/processed/testing_set_symbols_mapping.json'.format(currentFileDirectory)
TRAINING_SET_SYMBOLS_MAPPING = '{}/../data/processed/training_set_symbols_mapping.json'.format(currentFileDirectory)
WHOLE_DATA_SET_SYMBOLS_MAPPING = '{}/../data/processed/whole_data_set_symbols_mapping.json'.format(currentFileDirectory)
SYMBOLS_PARSING_CATEGORIES = '{}/../data/symbol_parsing_categories.json'.format(currentFileDirectory)



# Location of the folder where all the learned models are going to be persisted.
MODELS_FOLDER = '{}/../models/'.format(currentFileDirectory)

# Location of the folder all the evaluation results are going to be stored.
RESULTS_FOLDER = '{}/../results/'.format(currentFileDirectory)


PARSING_MODEL_LOCATION = lambda whole: "{}{}".format(MODELS_FOLDER, "{}{}".format('whole_' if whole else '', 'parsing_model.pkl'))

# Location of the folder all the evaluation results are going to be stored.
LG_OUTPUT_FILE = lambda datasetType, model, fileName: '{}/../results/graph_outputs/{}/{}/{}.lg'.format(currentFileDirectory, datasetType, model,fileName)

# Location of the folder all the evaluation results are going to be stored.
LG_OUTPUT_FOLDER = lambda datasetType, model: '{}/../results/graph_outputs/{}/{}'.format(currentFileDirectory, datasetType, model)

LG_SINGLE_EXPRESSIONS_OUTPUT_FOLDER = lambda model: '{}/../results/graph_outputs/single_expressions/{}'.format(currentFileDirectory, model)
LG_SEGMENTATION_PLUS_CLASSIFICATION_OUTPUT_FOLDER = lambda dataset:\
 lambda model:'{}/../results/graph_outputs/segmentation_plus_classification/{}/{}'.format(currentFileDirectory, dataset, model)


# Location of the folder all the evaluation results are going to be stored.
EVALUATION_OUTPUT_FOLDER = lambda datasetType: '{}/../results/evaluations/{}'.format(currentFileDirectory, datasetType)



GROUND_TRUTH_LG_OUTPUT_FILE = lambda datasetType, fileName: '{}/../results/graph_outputs/ground_truth/{}/{}.lg'.format(currentFileDirectory, datasetType, fileName)
GROUND_TRUTH_LG_FOLDER = lambda datasetType: '{}/../results/graph_outputs/ground_truth/{}/'.format(currentFileDirectory, datasetType)

# Location of the file that contain the list of all the unique symbol classes.
ALL_UNIQUE_SYMBOL_CLASSES_FILE = '{}/../data/all_unique_symbol_classes.json'.format(currentFileDirectory)

# Metadata for the instatiation of the classifiers.
ClassifierInfo = namedtuple('ClassifierInfo', ['name', 'classifierClass', 'params'])
CLASSIFIERS = [\

	ClassifierInfo
	(
		name = 'RANDOM_FOREST_CLASSIFIER',
		classifierClass = ensemble.RandomForestClassifier,
		params = {'n_estimators': 50}
	),

	# ClassifierInfo
	# (
	# 	name = '1_NN_CLASSIFIER',
	# 	classifierClass = cc.NearestNeighborClassifier,
	# 	params = {
	# 		'neighbors' : 1
	# 	}
	# )
	
	
]

# function that dinamically creates the the string of the path of a given model.
MODEL_FILE_NAME = lambda x:  "{}{}".format(MODELS_FOLDER, '{}_model.pkl'.format(x))