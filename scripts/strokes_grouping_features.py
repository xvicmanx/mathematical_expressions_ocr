"""
Authors: Kardo Aziz and Victor Trejo.
Description: This file contains all the functions related to features extraction
			 the relation between two pair of strokes.
			 With these features a probabilistic model is trained so the probability
			 of two traces belonging to the same group can be calculated.
"""
import sys
import utilities as utl
import project_constants as pc
import numpy as np
import normalizing_points_coordinates as npc
import entities
import normalizing_points_coordinates as norm
from sklearn.naive_bayes import GaussianNB

strokes = {}

def __calculateMinimumDistance(\
	currentTrace,
	otherTrace
):
	"""
	Calculates the minimum distance between two strokes.
	
	:param currentTrace: first trace involved to find the 
						 minimum distance.
	:type currentTrace: an entity Trace object.

	:param otherTrace: second trace involved to find the 
						 minimum distance.
	:type otherTrace: an entity Trace object.

	:return: the value of the minimum distance between the 
			 pair of traces.
	:rtype: float.
	"""
	currentTracePoints = currentTrace.getPoints()
	otherTracePoints = otherTrace.getPoints()
	
	minimumDistance = float("inf")
	for i in range(len(currentTracePoints)):
		for j in range(len(otherTracePoints)):
			minimumDistance = min(\
				minimumDistance,
				currentTracePoints[i].distance(
					otherTracePoints[j]
				)
			)

	return minimumDistance


def __calculateStartingPointsDistance(currentTrace, otherTrace):
	"""
	Calculates the distance between the starting points of two
	given traces.
	
	:param currentTrace: first trace involved to find the 
						starting points distances.
	:type currentTrace: an entity Trace object.

	:param otherTrace: second trace involved to find the 
						starting points distances.
	:type otherTrace: an entity Trace object.

	:return: the value of the distance between the traces
			 starting points.
	:rtype: float.
	"""
	currentTracePoints = currentTrace.getPoints()
	otherTracePoints = otherTrace.getPoints()
	distance =  currentTracePoints[0].distance(\
		otherTracePoints[0]
	)
	return distance







		
def __calculateBoundingBoxOverlap(currentTrace, otherTrace):
	"""
	Calculates the horizontal and vertical overlap between two given traces.
	
	:param currentTrace: first trace involved to find the overlaps.
	:type currentTrace: an entity Trace object.

	:param otherTrace: second trace involved to find the overlaps.
	:type otherTrace: an entity Trace object.

	:return: the values of the horizontal and vertical overlaps.
	:rtype: a pair of float.
	"""
	
	def calculateHorizontalOverlap(\
		first, second
	):
		if second.minPoint.x > first.minPoint.x and\
				 second.minPoint.x < first.maxPoint.x:
			overlap = min(first.maxPoint.x, second.maxPoint.x) - second.minPoint.x 
			minimumWidth = min(first.getWidth(), second.getWidth())
			minimumWidth = minimumWidth if minimumWidth > 0 else 1.0
			return overlap/minimumWidth
		return 0

	def calculateVerticalOverlap(\
		first, second
	):
		if second.minPoint.y > first.minPoint.y and\
				 second.minPoint.y < first.maxPoint.y:
			overlap = min(first.maxPoint.y, second.maxPoint.y) - second.minPoint.y 
			minimumHeight = min(first.getHeight(), second.getHeight())
			minimumHeight = minimumHeight if minimumHeight > 0 else 1.0
			return overlap/minimumHeight
		return 0

	
	currentBox = entities.BoundingBox.fromTuple(\
		npc.boundingBoxLimits([currentTrace])
	)
	otherBox = entities.BoundingBox.fromTuple(\
		npc.boundingBoxLimits([otherTrace])
	)

	horizontalOverlap = 0
	if currentBox.minPoint.x < otherBox.minPoint.x:
		horizontalOverlap = calculateHorizontalOverlap(currentBox, otherBox)
	else:
		horizontalOverlap = calculateHorizontalOverlap(otherBox, currentBox)


	verticalOverlap = 0
	if currentBox.minPoint.y < otherBox.minPoint.y:
		verticalOverlap = calculateVerticalOverlap(currentBox, otherBox)
	else:
		verticalOverlap = calculateVerticalOverlap(otherBox, currentBox)

	return horizontalOverlap, verticalOverlap
		




def __calculateBackwardMovement(currentTrace, otherTrace):
	"""
	Calculates the backward movement between two given traces.
	
	:param currentTrace: first trace involved to find the 
						backward movement.
	:type currentTrace: an entity Trace object.

	:param otherTrace: second trace involved to find the 
						backward movement.
	:type otherTrace: an entity Trace object.

	:return: the value of the backward movement
	:rtype: float.
	"""
	currentEndPoint = currentTrace.getPoints()[-1]
	otherStartPoint = otherTrace.getPoints()[0]

	#if otherStartPoint.x < currentEndPoint.x:
	return currentEndPoint.x - otherStartPoint.x
	# else:
	# 	return 0.0




def extractFeaturesBetweenTraces(currentTrace, otherTrace, medianLength):
	"""
	From a pair of features extracts features such as minimum distance,
	distance between starting points, horizontal-vertical overlap and
	backward movement.

	:param currentTrace: first trace.
	:type currentTrace: an entity Trace object.

	:param otherTrace: second trace.
	:type otherTrace: an entity Trace object.

	:param medianLength: median trace lenght of the traces belonging 
						 to the expression. This value will be used 
						 as feature normalizer.
	:type medianLength: float.

	:return: a row vector with the extracted features.
	:rtype: numpy array.

	"""
	norm.removeHooks([currentTrace,otherTrace])
	minimumDistance = __calculateMinimumDistance(\
		currentTrace,
		otherTrace
	)

	startingPointsDistance = __calculateStartingPointsDistance(\
		currentTrace,
		otherTrace
	)

	horizontalOverlap, verticalOverlap = __calculateBoundingBoxOverlap(\
		currentTrace,
		otherTrace
	)

	backwardMovement = __calculateBackwardMovement(\
		currentTrace,
		otherTrace
	)

	return np.hstack([\
		minimumDistance/medianLength,
		startingPointsDistance/medianLength,
		horizontalOverlap,
		verticalOverlap,
		backwardMovement/medianLength
	])
	

def trainProbabilisticModel(dataFiles, modelLocation):
	"""
	Trains a probabilistic models to determine the probability
	of two strokes belonging to the same group/symbol.
	
	:param dataFiles:  list of string paths of the files that 
						are going to be used to train the model.
	:type dataFiles: list of string.

	:param modelLocation:  location where the model will be stored.
	:type modelLocation: string.
	"""
	allFeatures, allTargets = [], []
	count = 0
	for trainFile in dataFiles:
		count+=1
		print("{} - {}".format(count, trainFile))
		expression = expressionFromLocation(trainFile)
		tracesToSymbolMap = {}
		traces = []
		
		for symbol in expression.getSymbols():
			tag = symbol.getTag()
			for t in symbol.getTraces():
				tracesToSymbolMap[t.getId()] = tag
				traces.append(t)
		
		medianLength = np.median([t.getLength() for t in traces])
		for i in range(len(traces)):
			currentTrace = traces[i]
			currentTraceSymbol = tracesToSymbolMap[currentTrace.getId()]
			rangeValue = min(\
				i + pc.STROKES_GROUPING_TO_CHECK_RANGE,
				len(traces)
			)
			
			for j in range(i+1, rangeValue):
				
				otherTrace = traces[j]
				otherTraceSymbol = tracesToSymbolMap[otherTrace.getId()]
				label = int(currentTraceSymbol == otherTraceSymbol)
				
				features = extractFeaturesBetweenTraces(\
					currentTrace,
					otherTrace,
					medianLength
				)

				allFeatures.append(np.hstack(features))
				allTargets.append(label)




	allFeatures = np.vstack(allFeatures)
	classifier = GaussianNB()
	classifier.fit(allFeatures, allTargets)
	utl.saveModelAtLocation(
		classifier,
		modelLocation
		
	)


def expressionFromLocation(location, parser = utl.parseINKML):
	"""
	Reads a mathetical expression at a given location.
	
	:param location:  location to read the expression from.
	:type location: string.

	:param parser:  parser that will read the inkml file, parse it and create
					the mathematical expression.
	:type parser: function.

	:return: the read mathematical expression.
	:rtype: MathematicalExpression.
	"""
	return parser(\
		utl.readFileFromLocation(\
			location
		)
	)



def maximumNumberOfStrokesPerSymbolStatistics(dataFiles):
	"""
	Finds what is the maximum number of strokes per symbol from 
	a given list of files representing mathematical expressions.

	:param dataFiles:  list of string paths of the files that 
						are going to be used to find the statistic.
	:type dataFiles: list of string.

	:return: the value of maximum number of stroke per symbol.
	:rtype: int.

	"""
	global strokes
	i = 0
	maxStrokesPerSymbol = 0
	for trainFile in dataFiles:
		print(i)
		i +=1
		expression = expressionFromLocation(trainFile)
		for s in expression.getSymbols():
			l = len(s.getTraces())
			if l in strokes :
				strokes[l] += 1
			else :
				strokes[l] = 1
		maxStrokesPerSymbol = max(\
			maxStrokesPerSymbol,
			max(
				[
					len(s.getTraces())
					for s in expression.getSymbols()
				]
			)
		)

	return maxStrokesPerSymbol


def main():
	"""
	Train probabilistic model on training and whole data.
	"""
	global strokes
	trainFiles =  utl.getTrainingFilePaths()
	testingFiles =  utl.getTestingFilePaths()
	
	# maximumNumberOfStrokesPerSymbolStatistics(trainFiles)
	# maximumNumberOfStrokesPerSymbolStatistics(testingFiles)

	# print("------------")
	# print(strokes)
	# print("------------")

	# Training probabilistic model on only training data
	trainProbabilisticModel(\
		trainFiles,
		pc.MODEL_FILE_NAME(
			pc.STROKES_GROUPING_PROBABILISTIC_MODEL
		)
	)

	# Training probabilistic model on whole data
	trainProbabilisticModel(\
		trainFiles + testingFiles,
		pc.MODEL_FILE_NAME(
			pc.WHOLE_DATA_STROKES_GROUPING_PROBABILISTIC_MODEL
		)
	)

if __name__ == "__main__":
	main()
	

