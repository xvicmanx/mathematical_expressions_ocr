"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script contains the implementation of the segmentation step.
"""
import numpy as np
import project_constants as pc
from bs4 import BeautifulSoup
import entities
import utilities as utl
import features_extraction as fe
import seg_alt as salt

debug = False
model = None
bestSegmentation = []
maximum_score = 0
scores_table = {}

def initialize(modelObject):
	global model
	model = modelObject
	salt.initialize(model)


class SegmentationConfig(object):
	"""docstring for SegmentationConfig"""
	def __init__(self, extractKey, scoreGroup):
		super(SegmentationConfig, self).__init__()
		self.extractKey = extractKey
		self.scoreGroup = scoreGroup
		


def extractKeyFromTraceGroup(traces):
	return (traces[0].getId(), traces[-1].getId())


def segment(traces):
	global bestSegmentation
	global maximum_score
	global scores_table
	global debug

	bestSegmentation = []
	maximum_score = 0
	scores_table = {}

	"""
	Segments a sequence a traces into a meaninful 
	groups that represent symbols.
	
	:param traces: a sequence of traces.
	:type traces: list of Trace objects.

	:return: the chosen segmentation.
	:rtype: a list of tuple of traces.
	"""
	#traces = [t.getId() for t in traces]
	configuration = SegmentationConfig(\
		extractKeyFromTraceGroup,
		classificationScore
	)

	
	findBestSegmentation(\
		[],
		traces,
		configuration
	)

	# if debug:
	# 	print("Traces : {}".format(len(traces)))
	# 	print("Best Segmentation")
	# 	printSegmentation(bestSegmentation)

	return bestSegmentation


def findInitialHypotheses(traces):
	"""
	Finds the possible segmentations that can 
	be obtained on the given traces sequence.
	
	:param traces: a sequence of traces.
	:type traces: list of Trace objects.

	:return: the possible segmentations.
	:rtype: a list of tuple of traces.
	"""
	numberOfTraces = len(traces)
	for k in range(2**(numberOfTraces-1)):
		binary =  np.binary_repr(\
			k,
			width = numberOfTraces-1
		)
		splitIndexes = [\
			(i+1)
			for i in range(len(binary))
			if int(binary[i]) > 0
		]

		
		yield tuple(\
			tuple(g)
			for g in np.split(traces, splitIndexes)
		)

def classificationScore(traces):
	features = fe.extractAllFeatures(traces)
	#print(features)
	re = model.predict_proba([features])
	#print(re)

	return np.max(re[0, :])


def calculateScore(segments, configuration):
	global scores_table 
	total = 1
	for group in segments :
		groupKey = configuration.extractKey(group)
		if groupKey in scores_table:
			total *= scores_table[groupKey]
		else :
			result = configuration.scoreGroup(\
				group
			)
			total *= result
			scores_table[groupKey] = result

	return total

def printSegmentation(segmentation):
	print "--------------------------"
	print "\nSegmentation : {"
	for g in segmentation:
		print "\n\tGroup : {"
		for i in g: 
			print("\t\t{}, ".format(i))

		print "\t},"
	print "\n}"
	print "--------------------------\n"


def copySegmentation(segmentation):
	result = []
	for g in segmentation:
		group = []
		for i in g: group.append(i)
		result.append(group)
	return result


def findBestSegmentation(segments, traces, configuration):
	global bestSegmentation
	global maximum_score
	global debug
	"""
	From the given set of proposed hypotheses,
	finds the best one.
	
	:param hypotheses: list of proposed hypotheses.
	:type hypotheses: list of tuple of traces

	:return: the best segmentations.
	:rtype: tuple of tuple of traces.
	"""

	segments = copySegmentation(segments)
	traces = np.copy(traces).tolist()
	
	if(len(traces) == 0):
		# a possibility
		# classify this segmentation
		# if it is greater than maximum_score
		# return maximum score
		score = calculateScore(segments, configuration)
		if score > maximum_score :
			maximum_score  =  score 
			bestSegmentation = copySegmentation(segments)
			# if debug:
			# 	printSegmentation(bestSegmentation)
		return



	if len(segments) == 0 and len(traces) != 0 :
		# First Iteration
		segments.append([traces[0]])
		traces.pop(0)
		findBestSegmentation(segments, traces, configuration)
	else:
		if len(traces) != 0 :
			next_Stroke =  traces[0]
			traces.pop(0)
			# not combined
			segments.append([next_Stroke])
			findBestSegmentation(
				segments,
				traces,
				configuration
			)
			segments.pop(-1)
			# combine it if it is less than 4 
			if(len(segments[-1]) != 4) :
				segments[-1].append(next_Stroke)
				findBestSegmentation(
					segments,
					traces,
					configuration
				)
			traces.insert(0,next_Stroke)




def parseINKMLWithCustomSegmentation(content):
	"""
	Parses an inkml file. with custom segmentation
	
	:param content: content of the inkml file
	:type content: string
	:return: a mathematical expression object created 
			 from information of the inkml file.
	:rtype: MathematicalExpression
	"""
	inkml = BeautifulSoup(content,'lxml')
	
	expression = entities.MathematicalExpression()

	# Extracting symbols information, traces, points 
	# from the xml file.
	def mapTraceFromTag(traceTag):
		points  = [\
			entities.Point.fromString(p)
			for p in traceTag.get_text().split(", ")
		]
		return entities.Trace(\
			traceTag['id'],
			points
		)

	traces =  map(\
		mapTraceFromTag, 
		__getTraceTags(inkml)
	)

	for group in salt.segment(traces):
		symbol = entities.Symbol('', '')
		
		for trace in group:
			symbol.addTrace(\
				trace.getId(),
				trace.getPoints()
			)	
		expression.addSymbol(symbol)

	# Returning the created mathematical expression.
	return expression


def __getTraceTags(inkml):
	"""
	Gets all the trace tags from a inkml content.
	
	:param inkml: inkml content object.
	:type inkml: BeautifulSoup.
	:return: a list of Beautiful XML tags
			 representing the trace tags.
	:rtype: Beautiful XML tag
	"""
	return inkml.find_all('trace')



if __name__ == "__main__":
	def extractKey(g):
		return "{}{}".format(g[0], g[-1])

	configuration = SegmentationConfig(\
		extractKey,
		lambda s: 1.0
	)
	
	traces = ['s1', 's2', 's3','s1', 's2', 's3', 's1', 's2', 's3','s1', 's2', 's3' ,'s1', 's2', 's3' ,'s1', 's2', 's3','s1', 's2', 's3']
	result = tuple(findInitialHypotheses(traces))
	print len(result)
	# findBestSegmentation([],traces, configuration)
	# printSegmentation(bestSegmentation) 

	# print(scores_table)
