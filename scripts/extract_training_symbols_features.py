"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script extracts features information
			 of the symbols from the expressions on the training set
			 and store it.
"""
import project_constants as pc
import utilities as utl
import numpy as np
import sys


def main(debug = False):
	"""
	Extracts features from the training raw data inkml files
	
	:param debug: to indicate performing debuging.
	:type debug: boolean.

	"""
	
	print("Extracting features from symbols for each mathematical expression (Training)...")
	
	filesLocations = map(\
		lambda f: "{}{}".format(pc.RAW_TRAIN_DATA_FOLDER, f),
		utl.directoryFiles(pc.RAW_TRAIN_DATA_FOLDER)
	)
	
	allFeatures, mapping = utl.extracFeaturesFromFilesLocations(filesLocations, debug)
	
	print(
		"Saving extracted features data: {} ...".format(
			pc.PROCESSED_TRAIN_DATA_SET_FILE
		)
	)

	utl.saveModelAtLocation(mapping, pc.TRAINING_SET_SYMBOLS_MAPPING)
	np.save(\
		pc.PROCESSED_TRAIN_DATA_SET_FILE,
		np.vstack(allFeatures)
	)


if __name__ == "__main__":
	debug = 'debug' in sys.argv
	main(debug)