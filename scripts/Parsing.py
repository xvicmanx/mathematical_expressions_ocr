"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script has the implementation of the parsing algorithm.
"""

import project_constants as pc
import utilities as utl
import strokes_grouping_features as sgf
import extract_relationship_features as erf

RIGH_RELATION = 'Right'
parsingModels =  None

def initializeModel(whole = False):
	"""
	Initializes the parsing model.
	
	:param whole: initializes the model trained on the whole data.
	:type whole: boolean.

	"""
	global parsingModels
	parsingModels = utl.readModelAtLocation(
		pc.PARSING_MODEL_LOCATION(whole)
	)



def __getStartSymbol(parsingSymbols):
	"""
	Gets the start symbol from the symbols sequence.
	
	:param parsingSymbols: aequences of symbols.
	:type parsingSymbols: list of Symbol.

	:return: the start symbol.
	:rtype: ParsingSymbol.

	"""
	if len(parsingSymbols) == 1: 
		return parsingSymbols[0]
	
	lastSymbol = parsingSymbols[-1]
	secondLastSymbol = parsingSymbols[-2]
	
	if lastSymbol.dominates(secondLastSymbol):
		parsingSymbols.remove(secondLastSymbol)
	else:
		parsingSymbols.remove(lastSymbol)

	return __getStartSymbol(parsingSymbols)



def isHorizontal(first, second):
	"""
	Checks if the relation between two symbols is horizontal.
	
	:param first: first parsing symbol
	:type first: ParsingSymbol.

	:param second: second parsing symbol
	:type second: ParsingSymbol.

	:return: is the relation is horizontal.
	:rtype: boolean.
	"""
	global parsingModels
	
	symbol = first.symbol
	otherSymbol = second.symbol

	features = erf.extractFeatures(\
		symbol,
		otherSymbol
	)

	symbolCategory = symbol.getParsingCategory()
	model = parsingModels[symbolCategory]
	relation = model.predict([features])[0]

	return relation == RIGH_RELATION


def __getBaseLine(startSymbol, symbols):
	"""
	Extracts the baseline from a given list of parsing symbols.
	
	:param startSymbol: start symbol in the baseline.
	:type startSymbol: ParsingSymbol.

	:param symbols: list of symbols.
	:type symbols:  list of ParsingSymbol.

	:return: the extracted baseline.
	:rtype: list of tuple (symbol, region symbols).
	"""
	baseLineSymbols = [startSymbol]
	results = []
	regionSymbols = []
	for symbol in symbols:
		lastInsertedSymbol = baseLineSymbols[-1]

		if (not lastInsertedSymbol.containsMinimumX(symbol)) \
			and isHorizontal(lastInsertedSymbol, symbol):
			results.append((lastInsertedSymbol,regionSymbols))
			baseLineSymbols.append(symbol)
			regionSymbols = []
		else :
			regionSymbols.append(symbol)

	results.append((baseLineSymbols[-1],regionSymbols))

	return results


relations = []
def parseRecursively(parsingSymbols):
	"""
	Extract relationships between symbols in list recursively.


	:param parsingSymbols: list of symbols.
	:type parsingSymbols:  list of ParsingSymbol.
	"""
	global relations

	#print([p.symbol.getGroundTruth() for p in parsingSymbols])
	
	startSymbol = __getStartSymbol([\
		s for s in parsingSymbols 
	])


	remainingSymbols = [\
		s for s in parsingSymbols 
		if s.symbol!=startSymbol.symbol
	]

	mainBaseLine = __getBaseLine(\
		startSymbol,
		remainingSymbols
	)

	# 101_alfans0
	for i in range(0,len(mainBaseLine)-1):
		relations.append([mainBaseLine[i][0],mainBaseLine[i+1][0],"Right"])

	# print([s[0].symbol.getGroundTruth() for s in mainBaseLine])
	for s, regions in mainBaseLine :
		#print(s.symbol.getGroundTruth())
		if len(regions)>0:
			#print("Regions : {}".format([r.symbol.getGroundTruth() for r in regions]))
			for r in regions :
				R = getRelation(s,r)
				# print("{} , {} , {}".format(s.symbol.getGroundTruth(),r.symbol.getGroundTruth(),R))
				s.insertIntoRelations(r,R)
			
			for regionName,regionSymbols in s.relations.items() :
				relations.append([s,regionSymbols[0],regionName])
				parseRecursively(regionSymbols)

expressionCounts = 0

def parse(symbols):
	"""
	Extract relationships between symbols in a list.
	
	:param symbols: list of symbols.
	:type symbols:  list of ParsingSymbol.

	:return: the extracted relationships.
	:rtype: list of tuple (symbol, otherSymbol, relationship).
	"""
	global expressionCounts
	expressionCounts+=1
	print("{}-Parsing Expression...".format(expressionCounts))
	global relations
	symbols = sorted(\
		symbols,
		key = lambda s: s.getMinX()
	)
	relations = []
	parseRecursively([\
		ParsingSymbol(s)
		for s in symbols
	])
	
	return relations


def getRelation(first, second):
	"""
	Extract relationship between two symbols.
	
	:param first: first symbol.
	:type first:  ParsingSymbol.

	:param second: second symbol.
	:type second:  ParsingSymbol.

	:return: the relationship label.
	:rtype: string.
	"""
	global parsingModels
	
	symbol = first.symbol
	otherSymbol = second.symbol

	features = erf.extractFeatures(\
		symbol,
		otherSymbol
	)

	symbolCategory = symbol.getParsingCategory()
	model = parsingModels[symbolCategory]
	return model.predict([features])[0]

class ParsingSymbol(object):
	"""docstring for ParsingSymbol"""
	def __init__(self, symbol):
		super(ParsingSymbol, self).__init__()
		self.symbol = symbol
		self.regions = {}
		self.relations = {}

	def insertIntoRegion(self, symbol, region):
		self.regions[region] = self.regions.get(region, [])
		self.regions[region].append(symbol)
	
	def insertIntoRelations(self, symbol, relation):
		self.relations[relation] = self.relations.get(relation, [])
		self.relations[relation].append(symbol)
	

	def containsCentroid(self, other):
		currentBox = self.symbol.getBox()
		otherBox = other.symbol.getBox()
		return currentBox.contains(otherBox.center)

	def containsCentroidX(self, other):
		currentBox = self.symbol.getBox()
		otherBox = other.symbol.getBox()
		return currentBox.isBetweenXRange(otherBox.center.x)
	
	def containsMinimumX(self, other):
		currentBox = self.symbol.getBox()
		otherBox = other.symbol.getBox()
		return currentBox.isBetweenXRange(otherBox.minPoint.x)

	def containsCentroidY(self, other):
		currentBox = self.symbol.getBox()
		otherBox = other.symbol.getBox()
		return currentBox.isBetweenYRange(otherBox.center.y)

	def isRoot(self):
		return "Root" == self.symbol.getParsingCategory()

	def isNonScripted(self):
		return "Operators-And-Relations" == self.symbol.getParsingCategory()

	def isVariableRange(self):
		return "Ranges" == self.symbol.getParsingCategory()

	def isBracket(self):
		category = self.symbol.getParsingCategory()
		return "Open-Brackets"==category or category == "Others"

	def contains(self, otherSymbol):
		return self.isRoot() and self.containsCentroid(otherSymbol)

	
	def isBefore(self, other):
		currentBox = self.symbol.getBox()
		otherBox = other.symbol.getBox()
		return currentBox.minPoint.x <= otherBox.minPoint.x

	def isWider(self, other):
		currentBox = self.symbol.getBox()
		otherBox = other.symbol.getBox()
		return currentBox.getWidth() > otherBox.getWidth()
	
	def overlaps(self, otherSymbol):
		mainCondition = self.isNonScripted() and\
		self.containsCentroidX(otherSymbol) and\
		not otherSymbol.contains(self)

		firstCondition = otherSymbol.isBracket() and\
			otherSymbol.containsCentroidY(self) and\
			otherSymbol.isBefore(self)

		secondCondition = (otherSymbol.isNonScripted() or\
			otherSymbol.isVariableRange()
		 ) and otherSymbol.isWider(self)

		return mainCondition and not (firstCondition or secondCondition)

	def isAdjacent(self, other):
		return isHorizontal(self, other) or\
				isHorizontal(other, self)

	def dominates(self, otherSymbol):
		return self.overlaps(otherSymbol) or\
				self.contains(otherSymbol) or\
				(self.isVariableRange() and\
					not otherSymbol.isAdjacent(self)
				 )



if __name__ == "__main__":
	initializeModel()
	testingFiles =  utl.getTestingFilePaths()
	for t in testingFiles:
		f = t.rfind('/')
		print("{} Main baseline: ".format(t[f+1:]))
		expression = sgf.expressionFromLocation(t)
		for  r in parse(expression.getSymbols()):
			print("R, {}, {}, {}, 1.0".format(r[0].symbol.getTag(),r[1].symbol.getTag(),r[2]))
		break

