"""
Authors: Kardo Aziz and Victor Trejo.
Description: This file contains all the functions related to features extraction
			 of the mathematical symbols.
"""
import numpy as np
import normalizing_points_coordinates as norm
import extract_global_features as gl
import extract_histogram_of_points as hp
import extract_histogram_of_orientation as ho

def extractAllFeatures(traces):
	"""
	Extract features from a symbol, 
	provided its traces information.
	
	:param traces: symbol's traces.
	:type traces: list of Trace objects.
	:return: a column vector representing the 
			 extracted features.
	:rtype: ndarray object
	"""
	
	# Preprocessing datata
	norm.preprocess(traces)
	
	# Getting all features
	globalfeatures = gl.getGlobalFeatures(traces)
	histogramOfPoints = hp.getHistogramofPoints(traces)
	histogramOfOrientations = ho.getHistogramofOrientation(traces)
	
	return np.array(\
		np.hstack(\
			(
				globalfeatures,
				histogramOfPoints,
				histogramOfOrientations
			)
		)
	)
