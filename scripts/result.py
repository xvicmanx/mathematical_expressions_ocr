"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script writes the result of the classification into a (.lg) file 
"""
import numpy as np
import Parsing as psng

def escapeInvalidChars(text):
	"""
	Escapes the characters that are considered invalid in the .lg
	files.

	:param text:  target text to escape invalid characters from.
	:type text:  string.

	:return : text after being escaped.
	:rtype : string

	"""

	escapingInvalidCharsMap = {
		',' : 'COMMA'
	}

	for key, value in escapingInvalidCharsMap.items():
		text = text.replace(key, value)

	return text

def makeDataToLGFormat(symbols, result):
	"""
	Organize the result and informatin of each symbol into LG file format
	Returns the organized format

	:param symbols:  all symbols metadata information needed to build the lg file
	:type symbols:  list of dictionary objects

	:param result: result of classification for each symbol in the expression
	:type result:  list of integers that presents the result

	:return : the lg format file
	:rtype : list of String

	"""
	formatedData = []
	formatedData.append("# Objects(" + str(len(symbols)) + ")")

	for i in range(len(symbols)):
		# for each trace in the symbol
		tagFromTraces = "{}:".format(":".join( symbols[i]['traces'].split(", ")))

		tagValue = escapeInvalidChars(symbols[i]['tag']) if len(symbols[i]['tag']) > 0 else tagFromTraces
		
		row = int(symbols[i]['row'])
		label = escapeInvalidChars(result[row])

		line = "O, {}, {}, 1.0, {}".format(tagValue, label, symbols[i]['traces'])
		
		formatedData.append(line)

	

	relationships = psng.parse([s['object'] for s in symbols])

	formatedData.append("\n# [ RELATIONSHIPS ]")
	formatedData.append("#\tObject Relationships (R): {}".format(len(relationships)))
	
	for firstSymbol, secondSymbol, relation in relationships:
		firstSymbolTracesIds = [t.getId() for t in firstSymbol.symbol.getTraces()]
		secondSymbolTracesIds = [t.getId() for t in secondSymbol.symbol.getTraces()]
		firstTagFromTraces = "{}:".format(":".join( firstSymbolTracesIds ))
		secondTagFromTraces = "{}:".format(":".join( secondSymbolTracesIds ))

		firstTag = firstSymbol.symbol.getTag()
		secondTag = secondSymbol.symbol.getTag()
		
		firstTagValue = escapeInvalidChars(firstTag) if len(firstTag) > 0 else firstTagFromTraces
		secondTagValue = escapeInvalidChars(secondTag) if len(secondTag) > 0 else secondTagFromTraces
		
		line = "R, {}, {}, {}, 1.0".format(firstTagValue, secondTagValue, relation)
		formatedData.append(line)
		
	return formatedData


def saveResultToLG(file_name,symbols,result):
	'''
	Create .lg file

	:param file_name: name of the inkml file without extention(.inkml)
	:param testData: the data that has been tested
	:type testData: a list of list that contains features for each symbol in the expression

	:param result: result of the test
	:type result: a list that contains the result for each symbol in the expression
	'''
	content = makeDataToLGFormat(symbols, result)
	with open(file_name, 'w') as file:
	    for item in content:
	        file.write("{}\n".format(item))
        
# if __name__ == "__main__":
# 	saveResultTolg('test', ["O, 2_a, 2, 1.0", "O, +_2, +, 1.0"])
