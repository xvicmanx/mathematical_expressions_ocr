"""
Author: Kardo Aziz and Victor Trejo 
Description: K-Nearest Neighbor implementation
"""

from pylab import *

class Classifier(object):
	"""Abstract Classifier"""
	def __init__(self):
		super(Classifier, self).__init__()
		
	def predict(self, inputData):
		"""
		Predicts the class of the  samples xInput

	    Keyword arguments:
	    	xInput -- sample whose class is going to be predicted.

	    Returns: an integer value that represents the 
	    		 class of the sample.
	    """
		raise NotImplementedError("This method should be implemented!")


	def fit(self, X, g):
		"""
		Trains the classifier.

	    Keyword arguments:
	    	X -- Training data input.
	    	g -- Training data output.
	    """
		raise NotImplementedError("This method should be implemented!")


class NearestNeighborClassifier(Classifier):
	"""Nearest Neighbor Binary Classifier implementation"""
	def __init__(self, neighbors):
		super(NearestNeighborClassifier, self).__init__()
		
		# Number of neighbors are going to be used 
		# to determine the class of a sample to be predicted
		self.k = neighbors


	def __toDummy(self, value, uniqueClasses):
		result = np.zeros(uniqueClasses)
		result[int(value)] = 1
		return result


	def fit(self, X, g):
		"""
		Trains the classifier.

	    Keyword arguments:
	    	X -- Training data input.
	    	g -- Training data output.
	    """
		# Training data input
		self.X = X
		# training data output (labels data)
		uniqueClasses = int(np.max(g)) + 1
		converter = lambda x: self.__toDummy(x, uniqueClasses)
		self.g = np.array(map(converter, g))
	


	def __oneSamplePredicion(self, x):
		"""
		Predicts the class of a given sample x

	    Keyword arguments:
	    	x -- sample whose class is going to be predicted.

	    Returns: an integer value that represents the 
	    		 class of the sample.
	    """
		# Calculating the Euclidean distance between the sample x
		# and all the samples from the training data X.
	
		d = np.sqrt( np.sum((self.X - x) ** 2, axis = 1) )	
		# Sorting the index of the samples by Euclidean distance
		# in increasing order.
		sortedIndexByDistance = argsort(d)
		# finding average value over all the output of 
		# the k neighbors
		y = np.apply_along_axis(np.sum, 0, self.g[sortedIndexByDistance[:self.k]])
	
		return np.argmax(y)


	def predict(self, xInput):
		"""
		Predicts the class of the  samples xInput

	    Keyword arguments:
	    	xInput -- sample whose class is going to be predicted.

	    Returns: an integer value that represents the 
	    		 class of the sample.
	    """
		
		return np.apply_along_axis(self.__oneSamplePredicion, 1, xInput)

	def __str__(self):
		return "{} - Nearest Neighbor Classifier".format(self.k)
