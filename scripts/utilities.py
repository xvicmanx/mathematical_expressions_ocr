"""
Authors: Kardo Aziz and Victor Trejo.
Description: This file contains general purpose functions that are used throughout the project.
"""

import project_constants as pc
from bs4 import BeautifulSoup
import entities
import pickle, os, json, ntpath
import numpy as np
import features_extraction as fe
import shutil, os

def customPrint(content, shouldPrint = True):
	"""
	Prints the passed content if the shouldPrint flag is set.

	:param content: content to be printed.
	:type content: string.
	
	:param shouldPrint: flag that specifies if the content 
						should be printed or not.
	:type shouldPrint: boolean.
	"""
	if shouldPrint: print(content)

def getTrainingFilePaths():
	"""
	Gets the path of all the training inkml files.

	:return: the list of inkml trainig files.
	:rtype: list of string
	"""
	return map(\
		lambda f: pc.TRAIN_INKML_FILE(f),
		directoryFiles(pc.RAW_TRAIN_DATA_FOLDER)
	)


def getTestingFilePaths():
	"""
	Gets the path of all the testing inkml files.

	:return: the list of inkml testing files.
	:rtype: list of string
	"""
	return map(\
		lambda f: pc.TEST_INKML_FILE(f),
		directoryFiles(pc.RAW_TEST_DATA_FOLDER)
	)


# To keep the models cached in memory so they 
# do not have to be read every time are needed.
models = {}
def loadModel(modelType):
	"""
	Loads the specied model.

	:param modelType: model type to load.
	:type modelType: string.

	:return: loaded model
	:rtype: scikit-learn classifier
	"""
	global models
	model = models.get(modelType, None)
	if model is None:
		model = readModelAtLocation(\
			pc.MODEL_FILE_NAME(modelType)
		)
		models[modelType] = model

	return model


def __getTraceGroupTags(inkml):
	"""
	Gets all the tracegroups from a inkml content.
	
	:param inkml: inkml content object.
	:type inkml: BeautifulSoup.
	:return: a list of Beautiful XML tags
			 representing the tracegroup tags.
	:rtype: Beautiful XML tag
	"""
	return inkml.tracegroup.find_all('tracegroup')


def  __getTruthFromTraceGroupTag(tag):
	"""
	Extracts the truth value from a tracegroup tag.
	
	:param tag: tracegroup tag whose truth value is 
				going to be extracted.
	:type tag: Beautiful XML tag.
	:return: the value of the ground truth.
	:rtype: string
	"""
	return tag.find(type = "truth").get_text()


def  __getTagLabelFromTraceGroupTag(tag):
	"""
	Extracts the href value from a tracegroup tag.
	
	:param tag: tracegroup tag whose tag label is 
				going to be extracted.
	:type tag: Beautiful XML tag.
	:return: the value of the tag label.
	:rtype: string
	"""
	annotation = tag.find('annotationxml')
	return annotation['href'] if not annotation is None else ''




def parseINKML(content):
	"""
	Parses an inkml file.
	
	:param content: content of the inkml file
	:type content: string
	:return: a mathematical expression object created 
			 from information of the inkml file.
	:rtype: MathematicalExpression
	"""
	inkml = BeautifulSoup(content,'lxml')
	traceGroupTags = __getTraceGroupTags(inkml)
	expression = entities.MathematicalExpression()

	# Extracting symbols information, traces, points 
	# from the xml file.
	for traceGroupTag in traceGroupTags:
		
		symbol = entities.Symbol(\
			__getTruthFromTraceGroupTag(traceGroupTag),
			__getTagLabelFromTraceGroupTag(traceGroupTag)
		)

		for traceView in traceGroupTag.find_all('traceview'):

			traceId = traceView['tracedataref']
			traceTag = inkml.find("trace", id = traceId)

			points  = [\
				entities.Point.fromString(p)
				for p in traceTag.get_text().split(", ")
			]

			symbol.addTrace(traceId, points)
		
		expression.addSymbol(symbol)

	# Returning the created mathematical expression.
	return expression





def getAllSymbolsFromInkml(content):
	"""
	Gets all the symbols classes from a inkml content.
	
	:param content: content of the inkml file
	:type content: string
	:return: a list of symbols classes strings.
	:rtype: list of string.
	"""
	inkml = BeautifulSoup(content,'lxml')
	return map(\
		lambda t: __getTruthFromTraceGroupTag(t),
		__getTraceGroupTags(inkml)
	 )

def readFileFromLocation(location):
	"""
	Reads the text content of a file at a specific
	location.
	
	:param location: location of the file to read.
	:type location: string
	:return: the read text content of the file.
	:rtype: string.
	"""
	with open(location.strip()) as f:
		return f.read()

def saveModelAtLocation(model, location):
	"""
	Design to store the classifier models.
	It stores an object at a specific location.
	
	:param model: object to be stored in file.
	:type model: object
	:param location: location to store the serialized object.
	:type location: string
	"""
	with open(location, 'w') as modelFile:
		pickle.dump(model, modelFile)


def readModelAtLocation(location):
	"""
	Design to read  the classifier models from their
	persisted files.
	It reads an object from a specific location.
	
	:param location: location to reads the object from.
	:type location: string
	:return: the object stored at the location.
	:rtype: object.
	"""
	with open(location) as modelFile:
		return pickle.load(modelFile)

def saveJsonAtLocation(JSONObject, location):
	"""
	Saves a JSON object at a specific location.
	
	:param JSONObject: JSON object to be stored.
	:type JSONObject: JSONObject
	:param location: location to store the JSON file.
	:type location: string
	:return: a json object created from content of the file .
	:rtype: JSON object.
	"""
	with open(location, 'w') as JSONFile:
		json.dump(JSONObject, JSONFile)


def readJsonAtLocation(location):
	"""
	Reads a JSON object from a file at a specific
	location.
	
	:param location: location of the JSON file to read.
	:type location: string
	
	:return: a json object created from content of the file .
	:rtype: JSON object.

	"""
	with open(location) as JSONFile:
		return json.load(JSONFile)

def directoryFiles(directory, extension = '.inkml'):
	"""
	Lists all the files at a given directory location.
	
	:param directory: location of the directory to list files
					  from.
	:type directory: string
	:return: a list of the file names at the given directory
			 location.
	:rtype: list of string

	"""
	return list([s for s in os.listdir(directory) if s.endswith(extension)])


def getClassifiersInformation():
	"""
	Returns the classifiers metada.

	:return: a list of of classifiers metadata.
	:rtype: list of ClassifierInformation

	"""
	return pc.CLASSIFIERS
	

def extracFeaturesFromFilesLocations(filesLocations, debug = False, inkmlFileParser = parseINKML):
	"""
	
	Extracts  features from the given list of inkml files locations.

	:param filesLocations: list of inkml file locations.
	:type filesLocations: list of string

	:param debug: perform debugging or not.
	:type debug: boolean

	:return: extracted features and expression metadata mapping.
	:rtype: two lists

	"""

	allFeatures = []
	mapping = []
	i = 0
	for location in filesLocations:
		
		if debug:
			print ("{}. {}".format(i, ntpath.basename(location)))
			i+=1

		content = readFileFromLocation(location)
		expression = inkmlFileParser(content)

		expressionInfo = {
			'name' : ntpath.basename(location).replace(".inkml", ""),
			'symbols' : []
		}

		for symbol in expression.getSymbols():
			traces = [
				t.getCopy() for t in symbol.getTraces()
			]
			symbolFeatures = fe.extractAllFeatures(symbol.getTraces())
			featuresPlusClass = np.hstack(\
				(
					symbolFeatures,
					symbol.getClassNumber()
				)
			)
			allFeatures.append(featuresPlusClass)
			
			symbol.updateTraces(traces)
			tracesIds = [t.getId() for t in symbol.getTraces()]

			expressionInfo['symbols'].append({\
				'row': len(allFeatures) - 1,
				'tag': symbol.getTag(),
				'traces': ", ".join(tracesIds),
				'object': symbol
			})

		mapping.append(expressionInfo)

	return allFeatures, mapping

def cleanDirectory(directory):
	"""
	Removes the content of a given directory

	:param directory: directory to clean.
	:type directory: string

	"""
	if os.path.exists(directory):
		for root, dirs, files in os.walk(directory):
			for f in files:
				os.unlink(os.path.join(root, f))
			for d in dirs:
				shutil.rmtree(os.path.join(root, d))

def createDirectory(directory):
	"""
	Creates a directory if does not exist.

	:param directory: directory to create.
	:type directory: string

	"""
	if not os.path.exists(directory):
		os.makedirs(directory)

        
if __name__ == "__main__":

	location = "{}{}".format(\
		pc.RAW_TRAIN_DATA_FOLDER,
		'2_em_3.inkml'
	)
	content = readFileFromLocation(location)
	parseINKMLWithCustomSegmentation(content).printContent()

