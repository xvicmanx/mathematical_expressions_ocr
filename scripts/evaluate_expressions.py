"""
Authors: Kardo Aziz and Victor Trejo.
Description: This script is used to evaluate expressions from a folder on the trained
classifiers.
"""
import sys, os
import evaluate_expression as evex
import utilities as utl
import project_constants as pc




def main(directory, evaluateOnWholeData = True, debug = False, performSegmentation = True):

	utl.customPrint("Loading model...", debug)

	model = utl.loadModel(pc.SEGMENTATION_CLASSIFIER_ID)

	if evaluateOnWholeData:
		model = utl.loadModel(\
			pc.WHOLE_DATA_SEGMENTATION_CLASSIFIER_ID
		)

	else:
		# Loading segmentation model
		model = utl.loadModel(\
			pc.SEGMENTATION_CLASSIFIER_ID
		)


		
	testFiles = map(\
		lambda f: os.path.join(directory, f),
		utl.directoryFiles(directory)
	)

	count = 0
	print("Evaluating testing set")
	for testFile in testFiles:
		count+=1
		utl.customPrint(\
			"{} - Evaluating expression at {}".format(count, testFile),
			debug
		)

		evex.main(\
			testFile,
			evaluateOnWholeData,
			model,
			debug,
			pc.LG_SINGLE_EXPRESSIONS_OUTPUT_FOLDER,
			performSegmentation
		)

if __name__ == "__main__":
	if len(sys.argv) > 1:
		directory = sys.argv[1]
		debug = 'debug' in sys.argv
		performSegmentation = not 'dnps' in sys.argv
		evaluateUsingClassifierTrainedOnWholeData = 'whole' in sys.argv
		main(directory, evaluateUsingClassifierTrainedOnWholeData, True, performSegmentation)