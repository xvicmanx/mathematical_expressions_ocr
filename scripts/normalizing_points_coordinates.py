"""
Authors: Kardo Aziz and Victor Trejo.

Description: This script normalize ( Pre-process )the points value of all traces in each symbol, 
makes all individual symbole on its own X AND Y coordinate

"""
import numpy as np
import entities, math
from scipy.interpolate import InterpolatedUnivariateSpline

def preprocess(traces):
	"""
	Preprocesses symbols' traces.
	It involves the following tasks:
		1. Interpolation of points.
		2. Hooks removal.
		3. Smoothing.
		4. Normalization.
	
	:param traces: symbols' traces.
	:type traces: list of Trace objects.

	"""
	# interpolatePoints(traces)
	removeHooks(traces)
	# smooth(traces)
	normalize(traces)


def smooth(traces):
	"""
	Smoothes symbols' traces by applying cubic spline interpolation
	over the sharp points.
	
	:param traces: symbols' traces.
	:type traces: list of Trace objects.

	"""

	# Iteration over all symbols' traces
	for t in traces:
		
		# Detecting sharp points
		sharpPoints = detectSharpPoints(t.getPoints())
		sharpPoints = sorted(sharpPoints, key=lambda p: p.x)

		# Continuing if the number of sharp points is less than 4
		if len(sharpPoints) <=3: continue
		
		
		xValues = [p.x for p in sharpPoints]
		yValues = [p.y for p in sharpPoints]

		# Finding interpolation cubic function over the sharp points
		interpolationFunction = InterpolatedUnivariateSpline(\
			xValues,
			yValues
		)

		# Generating new x values uniformly separated between the range
		# of the sharp points.
		newXValues = np.linspace(\
			min(xValues),
			max(xValues),
			num = 3 * len(t.getPoints()),
			endpoint = True
		)


		# For each new x point find a new y value using the 
		# interpolation function.
		newPoints = []
		for x in newXValues:
			y = interpolationFunction(x)
			if not np.isnan(y):
				newPoints.append(
					entities.Point(x, y)
				)
		
		# We are only updating the function if at least 
		# points the number of new valid points is equal to the number 
		# previous points.
		if len(newPoints) > len(t.getPoints()):
			t.updatePoints(newPoints)






def removeHooks(traces):
	"""
	Removes hooks from a symbol.
	Hooks are segment between two bordering sharp
	points.

	:param traces: symbols' traces.
	:type traces: list of Trace objects.

	"""

	def boundingBoxDiagonalLength(traces):
		min_X, min_Y, max_X, max_Y = boundingBoxLimits(traces)
		return np.sqrt((max_X - min_X)**2 + (max_Y - min_Y)**2)

	angleThreshold = np.pi/2
	lengthThreshold = 3.0 * boundingBoxDiagonalLength(traces)/100
	def __isAHook(firstSegment, secondSegment):
		"""
		Checks wether a bordering segment is a hook.
		It is a hook if its length is less or equals than 
		3 percent of the bounding box diagonal length.

		:param firstSegment: first segment.
		:type firstSegment: Line.

		:param secondSegment: second segment.
		:type secondSegment: Line.

		:return: if the bordering segment is a hook.
		:rtype: boolean

		"""
		return firstSegment.length() <= lengthThreshold and\
				firstSegment.angleBetween(secondSegment) <= angleThreshold

	# Iterating over all symbol's traces
	for t in traces:
		# Detecting sharp points
		sharpPoints = detectSharpPoints(t.getPoints())
		
		if len(sharpPoints) < 4: continue
		
		# Creating bordering segments


		beginningSegment = entities.Line(\
			sharpPoints[0],
			sharpPoints[1]
		)

		nextToBeginningSegment = entities.Line(\
			sharpPoints[1],
			sharpPoints[2]
		)

		endingSegment = entities.Line(\
			sharpPoints[-1],
			sharpPoints[-2]
		)

		beforeEndingSegment = entities.Line(\
			sharpPoints[-2],
			sharpPoints[-3]
		)

		# Checking if the bordering segments are 
		# hooks and therefore removing them.

		points = t.getPoints()
		if __isAHook(\
			beginningSegment,
			nextToBeginningSegment
		):
			endPointIndex = points.index(beginningSegment.endPoint)
			del points[:endPointIndex]
		
			

		if __isAHook(\
			endingSegment,
			beforeEndingSegment
		):
			endPointIndex = points.index(endingSegment.endPoint)
			del points[endPointIndex+1:]





def detectSharpPoints(points):
	"""
	Detects sharp points from a set of points.

	:param points: list of points to find sharp points from.
	:type points: list of Point objects.

	:return: a list of the sharp points found.
	:rtype: list of Point objects.

	"""
	numberOfPoints = len(points)
	
	if numberOfPoints <=0: return []
	
	sharpPoints = [points[0]]
	slopeAngles = [\
		points[i].slopeAngle(points[i+1])
		for i in range(numberOfPoints - 1)
	]

	previousAnglesDifference, i = 0, 0
	while i  < len(slopeAngles) - 1:
		anglesDifference = slopeAngles[i] - slopeAngles[i+1]
		if anglesDifference == 0: 
			i+=1
			continue

		delta = anglesDifference * previousAnglesDifference
		
		if delta <=0 and previousAnglesDifference != 0:
			sharpPoints.append(points[i+1])
		i+=1
		previousAnglesDifference = anglesDifference

	sharpPoints.append(points[-1])
	
	return sharpPoints





def interpolatePoints(traces):
	"""
	Interpolate points in the traces.
	This interpolation is made by inserting points in places
	where the distance/gap is larger than a maximum threshold value
	which is a value proportional to the average point to point separation.

	:param traces: symbols' traces.
	:type traces: list of Trace objects.

	"""

	def createInsertingPoint(currentPoint, nextPoint, distance):
		"""

		Creates an interpolating point.
		
		"""
		
		if currentPoint.x != nextPoint.x:
			slope = currentPoint.slope(nextPoint)

		newX = currentPoint.x
		if currentPoint.x < nextPoint.x:
			newX = currentPoint.x + np.sqrt(distance**2/(slope**2+1))
		elif currentPoint.x > nextPoint.x:
			newX = currentPoint.x - np.sqrt(distance**2/(slope**2+1))
		
		if currentPoint.x != nextPoint.x:
			newY = slope*newX + currentPoint.y - slope*currentPoint.x
		elif currentPoint.y < nextPoint.y and currentPoint.x == nextPoint.x:
			newY = currentPoint.y + distance
		elif currentPoint.y > nextPoint.y and currentPoint.x == nextPoint.x:
			newY = currentPoint.y - distance
		
		return entities.Point(newX, newY) 
		

	
	for t in traces:
		points  = t.getPoints()
		distances = [\
			points[i].distance(points[i-1])
			for i in range(1, len(points))
		]

		if len(distances) <= 0: continue
		
		averageDistance = np.average(distances)
		
		d = 0.7 * averageDistance
		i = 0
		while i < (len(points) - 1):
			currentPoint = points[i]
			nextPoint = points[i+1]
			l =  currentPoint.distance(nextPoint)
			if l > d:
				newPoint = createInsertingPoint(currentPoint, nextPoint, d)
				points.insert(i+1,  newPoint)
			i+=1






def boundingBoxLimits(traces):
	"""
	Gets the bounding box  of a symbol represented
	by a list of Trace objects.
	
	:param traces: symbols' traces.
	:type traces: list of Trace objects.

	:return: the bounding box values 
			 (min x, min y, max x, max y).
	:rtype: a 4 float tuple

	"""

	min_X = float("inf")
	min_Y = float("inf")
	max_X = float("-inf")
	max_Y = float("-inf")
	for t in traces:
		points = t.getPoints()
		for p in points:
			max_Y = max(max_Y,p.y)
			max_X = max(max_X,p.x)
			min_X = min(min_X,p.x)
			min_Y = min(min_Y,p.y)

	return min_X, min_Y, max_X, max_Y





def normalize(traces):
	"""
	Normalizes the points of a symbol represented
	by a list of Trace objects.
	
	:param traces: symbols' traces.
	:type traces: list of Trace objects.

	"""
	min_X, min_Y, max_X, max_Y = boundingBoxLimits(traces)

	# First Transiting the symbole in a way that coordinates start with (0,0) to (w,h)
	# where w : is width of the symbole and h: is the height of the symbol
	for t in traces:
		points = t.getPoints()
		for p in points:
			p.update(((p.x -abs(min_X))), (p.y - abs(min_Y)))

	

	max_X -= min_X
	max_Y -= min_Y
	min_X = 0
	min_Y = 0
	if(max_X== 0 and max_Y == 0):
		for t in traces :
			for p in t.getPoints():
				p.update(2,2)
	else:
		width = max_X 
		height = max_Y 
		centerizeX = 0
		centerizeY = 0
		# Then scale the symbole into a grid of 4X4 without distoring the shape or aspect ratio
		# first calculating the scalling ratio
		if height>=width:
			ratio = 4.0/height
			centerizeX = (4-(ratio * max_X))/2.0
		else:
			ratio = 4.0/width
			centerizeY = (4-(ratio * max_Y))/2.0
		# then multiply that ratio by each point in the symbol
		# centerizing parameter is just makes sure that the symbol stays in the center of the grid
		for t in traces:
			points = t.getPoints()
			for p in points:
				p.update((p.x*ratio)+centerizeX, (p.y*ratio)+centerizeY)


# if __name__ == "__main__":
# 	p1 = Point(-20,10)
# 	p2 = Point(20,10)
# 	p3 = Point(-10,20)
# 	p4 = Point(-10,-20)

# 	points = [p1,p2]
# 	points2 = [p3,p4]
# 	t = Trace(0,points)
# 	t2 = Trace(0,points2)
# 	normalize([t,t2])
# for p in t.getPoints() :
# 	print(p)
# for p in t2.getPoints() :
# 	print(p)