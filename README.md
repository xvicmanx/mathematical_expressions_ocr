# README #

This project is the implementation of recognizer of mathematical expressions.

### Contribuitors ###

* Kardo Aziz
* Victor Trejo


### Structure of the project ###

- data : contains all the data used throughtout the project.

	* processed : contains the features extracted information.
		* training_set.py : training data extracted features.

	* raw : contains the inkml files.
		* train : contains the inkml training files.
		* test : contains the inkml testing files.

	* all_unique_symbol_classes.json : This file contains the map of each symbol class label to a numerical value to make it easier to our models when using the data.

- models  : contains the trained model files.

- results : contains the evaluation results.

- scripts : contains the project scripts

	* create_ground_truth_to_number_map.py : This script finds all the unique symbols classes in all the mathetical expression and stored them in to a json file.
	
	* entities.py : This file contains entities classes and functions declarations for the current domain of mathematical expressions.

	* extract_training_symbols_features.py : This script extracts features information of the symbols from the expressions on the training set and store it.

	* extract_testing_symbols_features.py : This script extracts features information of the symbols from the expressions on the testing set and store it.

	* features_extraction.py : This file contains all the functions related to features extraction of the mathematical symbols.

	* extract_global_features.py : This file contains all the functions related to the extracting global features.

	* normalizing_points_coordinates.py : This script contains the  functions used to preprocess and normalize the points of the symbol traces.

	* extract_histogram_of_orientation.py : This script contains the functionality to extract the histogram of orientation features.

	* extract_histogram_of_points.py : This script contains the functionality to extract the histogram of points features.

	* project_constants.py : This file contains the project constants values and values functions.

	* train_classification_models.py : This script trains the classifiers using the features information of the symbols from the expressions on the training set or the whole data set if the argument 'whole' is passed when running the script.

	* utilities.py : This file contains general purpose functions that are used throughout the project.

	* generate_output_evaluation.py : This file generates the evaluation (summary and confusion matrix) for a model and saves result in the evalauation folder.

	* result.py : This file saves the classification result of a (.inkml) file as (.lg) in the result/graph_output/test/model_name folder

	* generate_classifier_output_graphs_on_datasets.py :  This script tests the trained model on the  features data and stores the result as '.lg'  for each '.inkml' file.

	* generate_truth_output_graphs.py :  This script generates ground truth '.lg' files from the data inkml files of each data set (raw folder).
	
	* custom_classifiers.py :  This script contains our implementation of the K-Nearest-Neighbor classifier.

	* strokes_grouping_features.py: This file contains all the functions related to features extraction the relation between two pair of strokes. With these features a probabilistic model is trained so the probability of two traces belonging to the same group can be calculated.

	* segmentation.py: This script contains the implementation of the segmentation step.

	* cross_validate_model.py: This script uses grid search to find the best parameters to tune our classifier.  

	* evaluate_expression.py :  This script reads an inkml file and evaluates on each of the train classifiers generating an '.lg' file for each one of them. The results are stored in   under evaluations/graph_outputs/single_expression/{classifier_name}/{expression_file_name}.lg
	This script takes two arguments one is mandatory (the location of the inkml file)  and the other is optional (the flag 'whole' which indicates if the model trained on the whole data will be used instead.)



	* evaluate_expressions.py :  This script reads an inkml file and evaluates on each of the train classifiers generating an '.lg' file for each one of them. The results are stored in   under evaluations/graph_outputs/single_expression/{classifier_name}/{expression_file_name}.lg
	This script takes three arguments one is mandatory (the location of the folder of inkml files)  and the others are optional (the flag 'whole' which indicates if the model trained on the whole data will be used instead; and 'dnps' to evaluate expression without performing segmentation and classifying the symbols in isolation)


	* Parsing.py : This script has the implementation of the parsing algorithm.

	* initialize.py: Performs the basic initialization tasks:
		1. Creating ground truth to symbol ids map.
		2. Generating ground truth output graphs.
		3. Extracting features from training data set inkml files.
		4. Extracting features from testing data set inkml files.

	* train_and_evaluate_models.py :Performs the following tasks:
		1. Training classification models.
		2. Generating output graphs of classifiers on datasets.
		3. Generating evaluation of the classifiers output graphs
		   against the ground truths graphs.

### How the project works? ###

- Where data should be located:
	* The inkml data should be put under the directory data/raw. The folder for each dataset needs to be put over there with the names 'train' and 'test' respectively. This folder will contain the inkml files.

- How to use the scripts to replicate our results:

	1. Run the python script *initialize.py* to extract data features.

	2. Run the python script *train_classification_models.py* to train the classifiers using the data from *training_set.npy* and store the models into the *models* folder. If an optional argument is passed ('whole') it also train the classifiers on the whole data  set. The models are stored under the folder 'models' in a pickle file with the following format name: {classifier_name}.pkl
	or whole_{classifier_name}.pkl if the classifier was trained on whole dataset.

	3. Run the python script *generate_classifier_output_graphs_on_datasets.py* to use the classifier to generate the output graphs for each dataset. This will only used the classification models trained on the train data not the whole data. This results will be stored under the directory:
	results/graph_outputs/{DATASET_TYPE}/{CLASSIFIER_NAME}. DATASET_TYPE can be either train or test.
	CLASSIFIER_NAME will be depending on the classifier's name.

	4. Run the python script *generate_output_evaluation.py* to evaluate the output graphs generated by the classifiers against the output truth. This results are stored under the folder results/evaluations/{DATA_SET_TYPE}. DATASET_TYPE which is the dataset the classifier output were compared against, can be either train or test. Under this folder outputs from the 'Lgeval evaluate' and 'LgEval confHist' will be stored for each classifier. 


- How to use the scripts (short way) to replicate our results:

	The previous process can be done in only two steps:


	1. Run the python script *initialize.py* to extract data features.
	2. Run the python script *train_and_evaluate_models.py* to train and evaluate the models.
		This script will call the scripts for steps 2, 3 and 4. It receives an optional parameter 'whole' to train the the models on the whole data set.


- How to evaluate a single expression on the trained models:
	1. Run the python script  *evaluate_expression.py* passing as first argument the location of the inkml. This will stored the results   under evaluations/graph_outputs/single_expression/{classifier_name}/{expression_file_name}.lg. This script also has an  optional argument (the flag 'whole' which indicates if the model trained on the whole data will be used instead.)


- How to evaluate multiple expressions on the trained models:
	1. Run the python script  *evaluate_expressions.py* passing as first argument the location of folder of the inkml files. This will stored the results   under evaluations/graph_outputs/single_expression/{classifier_name}/{expression_file_name}.lg. This script also has two  optional arguments (the flag 'whole' which indicates if the model trained on the whole data will be used instead and the flag 'dnps' to evaluate expression without performing segmentation and classifying the symbols in isolation)
	