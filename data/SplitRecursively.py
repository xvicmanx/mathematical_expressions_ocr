from pylab import *
import re
import sys
import math
import pickle
import os
sys.path.append('/RIT/Spring_2016/Pattern_Recognition/project1/crohmelib/bin')
from inkml import *
minimum = float("inf")
testsize = 0;
def get_List_of_File(filename):
	'''
	Read the file that contain names of the other files to be read
	'''
	# Read the File into a list 
	files = open(filename,'r').readlines()
	# return list of name of the files
	return files

def splitRecursive(train,test,required):
	global testsize
	if len(test) == testsize :
		distribution = getDistribution(test)
		if len(distribution) == 101:
			difference = getDifference(distribution,required)
			if(difference < minimum):
				pickle.dump( test, open( "testProject.p", "wb" ) )
	else:
		first_file = train.keys()[0]
		test[first_file] = train[first_file]
		train.pop(first_file)
		splitRecursive(train,test,required)

		train[first_file] = test[first_file]
		test.pop(first_file)
		splitRecursive(train,test,required)

def getDistribution(dataset):
	distribution = {}
	for file in dataset :
		symbols = dataset[file]
		for s in symbols:
			if s not in distribution:
				distribution[s] = 1
			else:
				distribution[s] += 1
	return distribution
def getDifference(distribution,required):
	difference = 0
	for s in required:
		difference += abs(distribution[s]-required[s])
	return difference




if __name__ == "__main__":
	global testsize
	files = get_List_of_File("TrainINKML_v3-2/AllEM_part4_TRAIN_all.txt")
	f = int(len(files))
	sys.setrecursionlimit(9000)

	# File_and_symbols = {}
	# test ={}
	# Distribution = {}
	# requiredDistribution = {}

	print("Total file number : "+str(f))
	testsize = int(f *(1/3.0))
	print("Files to be in Test : "+str(testsize))

	# for m in range(0,len(files)):
	# 	#print(files[m])
	# 	try:
	# 		f = Inkml("TrainINKML_v3-2/"+files[m].strip())
	# 		symbols = []
	# 		for s in f.segments.values():
	# 			symbols.append(s.label)
	# 			if s.label not in Distribution:
	# 				Distribution[s.label.strip()] = 1
	# 			else:
	# 				Distribution[s.label.strip()] += 1
	# 		File_and_symbols[files[m]] = symbols
	# 	except IOError:
	# 		print "Can not open " + files[m].strip()
	# 	except ET.ParseError:
	# 		print "Inkml Parse error " + files[m].strip()
	

	# print("Number of class : {}".format(len(Distribution)))

	# for key in Distribution :
	# 	requiredDistribution[key] = (1/3.0)* Distribution[key]

	# print("Number of class in Required : {}".format(len(requiredDistribution)))
	#splitRecursive(File_and_symbols,test,requiredDistribution)

	test = pickle.load(open('testFile_names.p','rb'))

	currentFileDirectory = os.path.realpath('.')
	
	source = "TrainINKML_v3-2/"
	Testdestination = "/RIT/Projetesct1/mathematical_expressions_ocr/data/raw/test"
	Traindestination = "/RIT/Project1/mathematical_expressions_ocr/data/raw/train"
	
	TestFiles = []
	TrainFiles = []
	
	for i in test :
		TestFiles.append(currentFileDirectory +"/TrainINKML_v3-2/"+ i.strip())
	
	for f in files :
		if currentFileDirectory +"/TrainINKML_v3-2/"+f.strip() not in TestFiles :
			TrainFiles.append(currentFileDirectory +"/TrainINKML_v3-2/"+ f.strip())
	print(len(TestFiles))
	print(len(TrainFiles))
		
	os.chdir(currentFileDirectory+"/raw/test")
	with open("filenames.txt", 'w') as file:
	    for item in TestFiles:
	        file.write("{}\n".format(item))

	os.chdir(currentFileDirectory+"/raw/train")
	with open("filenames.txt", 'w') as file:
	    for item in TrainFiles:
	        file.write("{}\n".format(item))
	
	# Moving files to Test Folder
	newTestfilename = []
	os.chdir(currentFileDirectory+"/raw/test")
	currentDirectory = os.getcwd()
	print(currentDirectory)
	for source in TestFiles:
		os.system("cp {} {}".format(source,currentDirectory))
		newTestfilename.append(currentDirectory + source[source.rindex('/'):])
	# create a file to save names
	with open("filenames.txt", 'w') as file:
	    for item in newTestfilename:
	        file.write("{}\n".format(item))

	# Moving Files to Train Folder
	newTrainfilename = []
	os.chdir(currentFileDirectory+"/raw/train")
	currentDirectory = os.getcwd()
	for source in TrainFiles:
		os.system("cp {} {}".format(source,currentDirectory))
		newTrainfilename.append(currentDirectory + source[source.rindex('/'):])
	# Create a file to save names
	with open("filenames.txt", 'w') as file:
	    for item in newTrainfilename:
	        file.write("{}\n".format(item))
	# achieved = getDistribution(test)
	# for key in requiredDistribution :
		
	# 	print("{} : ".format(key))
	# 	print("Required : {}".format(requiredDistribution[key]))
	# 	print("Achieved : {}".format(achieved[key]))




